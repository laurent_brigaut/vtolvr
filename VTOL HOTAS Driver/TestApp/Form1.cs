﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace TestApp
{
    public partial class Form1 : Form
    {
        private VTOLHOTASDriver.Main _ctrl;
        private StdOutLogger _logger = new StdOutLogger();

        private DispatcherTimer _timer = new DispatcherTimer();

        public Form1()
        {
            InitializeComponent();

            _ctrl = new VTOLHOTASDriver.Main();
            _ctrl.Initialize(_logger);

            lbx_StickList.DataSource = _ctrl.GetPhysicalcontrollers();
            lbx_StickList.DisplayMember = "name";

            dgv_Actors.DefaultCellStyle.SelectionBackColor = dgv_Actors.DefaultCellStyle.BackColor;
            dgv_Actors.DefaultCellStyle.SelectionForeColor = dgv_Actors.DefaultCellStyle.ForeColor;

            //dgv_Actors.RowsDefaultCellStyle.SelectionBackColor = System.Drawing.Color.Transparent;
            dgv_Actors.AutoGenerateColumns = false;
            dgv_Actors.Columns.Clear();
            DataGridViewColumn col = new DataGridViewTextBoxColumn();
            col.DataPropertyName = "paName";
            col.HeaderText = "Physical Name";
            col.Name = "Physical Name";
            dgv_Actors.Columns.Add(col);

            col = new DataGridViewTextBoxColumn();
            col.DataPropertyName = "paType";
            col.HeaderText = "Physical Type";
            col.Name = "Physical Type";
            dgv_Actors.Columns.Add(col);

            col = new DataGridViewTextBoxColumn();
            col.DataPropertyName = "paValue";
            col.HeaderText = "Physical Value";
            col.Name = "Physical Value";
            dgv_Actors.Columns.Add(col);

            col = new DataGridViewTextBoxColumn();
            col.DataPropertyName = "vaName";
            col.HeaderText = "VR Name";
            col.Name = "VR Name";
            dgv_Actors.Columns.Add(col);

            col = new DataGridViewTextBoxColumn();
            col.DataPropertyName = "vaValue";
            col.HeaderText = "VR Value";
            col.Name = "VR Value";
            dgv_Actors.Columns.Add(col);



            // Start timer proc used to simulate Unity Update proc
            _timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(100) };
            _timer.Tick += _timerProc;
            _timer.Start();

        }

        private void lbx_StickList_SelectedValueChanged(object sender, EventArgs e)
        {
            VTOLHOTASDriver.IPhysicalController ctrl = (VTOLHOTASDriver.IPhysicalController)((ListBox)sender).SelectedItem;
            dgv_Actors.DataSource = ctrl.GetMatchedActors();
        }
        void _timerProc(object sender, EventArgs e)
        {
            ArrayList sticks = _ctrl.GetPhysicalcontrollers();
            foreach (var stick in sticks)
            {
                ((VTOLHOTASDriver.IPhysicalController)stick).updateActors();
                
                if (stick == lbx_StickList.SelectedItem)
                {
                    //dgv_Actors.DataSource = null;
                    dgv_Actors.DataSource = ((VTOLHOTASDriver.IPhysicalController)stick).GetMatchedActors();
                }
            }
        }

      }
}
