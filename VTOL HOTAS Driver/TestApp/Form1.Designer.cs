﻿
namespace TestApp
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbx_StickList = new System.Windows.Forms.ListBox();
            this.dgv_Actors = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Actors)).BeginInit();
            this.SuspendLayout();
            // 
            // lbx_StickList
            // 
            this.lbx_StickList.FormattingEnabled = true;
            this.lbx_StickList.Location = new System.Drawing.Point(23, 21);
            this.lbx_StickList.Name = "lbx_StickList";
            this.lbx_StickList.Size = new System.Drawing.Size(647, 108);
            this.lbx_StickList.TabIndex = 0;
            this.lbx_StickList.SelectedValueChanged += new System.EventHandler(this.lbx_StickList_SelectedValueChanged);
            // 
            // dgv_Actors
            // 
            this.dgv_Actors.AllowUserToAddRows = false;
            this.dgv_Actors.AllowUserToDeleteRows = false;
            this.dgv_Actors.AllowUserToResizeRows = false;
            this.dgv_Actors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Actors.Location = new System.Drawing.Point(23, 136);
            this.dgv_Actors.MultiSelect = false;
            this.dgv_Actors.Name = "dgv_Actors";
            this.dgv_Actors.ReadOnly = true;
            this.dgv_Actors.RowHeadersVisible = false;
            this.dgv_Actors.Size = new System.Drawing.Size(647, 302);
            this.dgv_Actors.TabIndex = 1;
           
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 512);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgv_Actors);
            this.Controls.Add(this.lbx_StickList);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Actors)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbx_StickList;
        private System.Windows.Forms.DataGridView dgv_Actors;
        private System.Windows.Forms.Button button1;
    }
}

