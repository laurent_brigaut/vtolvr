﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VTOLHOTASDriver;

namespace TestApp
{
    class StdOutLogger : VTOLHOTASDriver.ILogger
    {
        public void _Log(object message, LogLevel level = LogLevel.info, LogDestination dest = LogDestination.log_file)
        {
            Debug.WriteLine(message);
        }
     }
}
