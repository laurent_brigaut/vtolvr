﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTOLHOTASDriver
{
    public class BasePhysicalActor : IPhysicalActor
    {
        public string name { get; private set; }
        public int value { get; set; }
        public PhysicalActorTypes type { get; protected set; }

        public BasePhysicalActor(string name)
        {
            this.name = name;
        }

        public virtual object ValueToVR()
        {
            return value;
        }
    }
}
