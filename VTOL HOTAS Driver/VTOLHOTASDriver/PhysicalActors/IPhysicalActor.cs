﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    public enum PhysicalActorTypes
    {
        None,
        Axis,
        Slider,
        Button,
        POV,
        POVButtons,
    }
    public interface IPhysicalActor
    {
        string name { get; }
        int value { get; set; }
        PhysicalActorTypes type { get; }
        object ValueToVR();
    }
}
