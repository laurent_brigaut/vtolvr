﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    public class POVActor : BasePhysicalActor
    {
        private static readonly Dictionary<int, Vector3> PovAngleToVector3 = new Dictionary<int, Vector3>
        {
            { -1, new Vector3(0, 0, 0) },       //Center 
            { 0, new Vector3(0, 1, 0) },        // Up
            { 4500, new Vector3(1, 1, 0) },     // Up Right
            { 9000, new Vector3(1, 0, 0) },     // Right
            { 13500, new Vector3(1, -1, 0) },   // Down Right
            { 18000, new Vector3(0, -1, 0) },   // Down
            { 22500, new Vector3(-1, -1, 0) },  // Down Left
            { 27000, new Vector3(-1, 0, 0) },   // Left
            { 31500, new Vector3(-1, 1, 0) }    // Up Left
        };


        public POVActor(string name):base(name)
        {
            type = PhysicalActorTypes.POV;
        }


        public override object ValueToVR()
        {
            Vector3 v = new Vector3(0,0,0);

            PovAngleToVector3.TryGetValue(value, out v);

            return v;
        }
    }
}
