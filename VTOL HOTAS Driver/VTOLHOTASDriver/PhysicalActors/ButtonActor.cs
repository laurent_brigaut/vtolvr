﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTOLHOTASDriver
{
    public class ButtonActor : BasePhysicalActor
    {

        public ButtonActor(string name) : base(name)
        {
            type = PhysicalActorTypes.Button;
        }

        public override object ValueToVR()
        {
            float v = 0;
          
            if (value == 0) v = (float)0.0; else v = (float)1.0;

            return v;
           
        }
    }
}
