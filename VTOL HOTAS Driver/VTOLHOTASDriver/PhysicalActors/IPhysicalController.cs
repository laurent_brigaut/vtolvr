﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTOLHOTASDriver
{
    public interface IPhysicalController
    {
        string name { get; }
        bool isLoaded { get; }
        ILogger logger { get; }


        void matchActors(IPhysicalActor physical, IVRActor vr);
        ArrayList GetMatchedActors();

        void updateActors();
    }
}
