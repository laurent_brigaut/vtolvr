﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTOLHOTASDriver
{
    public class AxisActor : BasePhysicalActor
    {
        public bool invert {get; set;}

        public AxisActor(string name, bool invert) : base(name)
        {
            this.invert = invert;
            type = PhysicalActorTypes.Axis;
        }

        
        public override object ValueToVR()
        {
           float retVal = 0;

            if (value == 65535) retVal = 1;
            else retVal = (((float)value / 32767) - 1);
            
            if (invert) retVal *= -1;

            return retVal;
            
        }
    }
}
