﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTOLHOTASDriver
{
    class SliderActor : AxisActor
    {
        public SliderActor(string name, bool invert) : base(name, invert)
        {
            type = PhysicalActorTypes.Slider;
        }


        public override object ValueToVR()
        {
            float retVal = 0;

            if (value == 65535) retVal = 1;
            else retVal = ((float)value / 65535);

            if (invert) retVal = 1 - retVal;

            return retVal;

        }
    }
}
