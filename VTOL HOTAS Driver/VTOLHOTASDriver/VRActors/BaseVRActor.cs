﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    class BaseVRActor : IVRActor
    {
        

        public string name { get; protected set; }

        public object value { get; protected set; }

        public IVRComponent parent { get; protected set; }
        public string V { get; }


        public BaseVRActor(string n)
        {
            name = n;
            value = (float)0.0;
        }

        public void setParent(IVRComponent p)
        {
            parent = p;
        }

        public void setValue(float v)
        {
            float oldValue;

            oldValue = (float)this.value;
            this.value = (float)v;

            if (parent != null) parent.ValueChanged(this, oldValue);
        }

        public void setValue(Vector3 v)
        {
            Vector3 oldValue;
            Vector3 tempValue;

            if (value is float) value = new Vector3(0, 0, 0);

            tempValue = (Vector3)value;

            oldValue = new Vector3(tempValue.x, tempValue.y, tempValue.z);
            this.value = new Vector3(v.x, v.y, v.z);

            if (parent != null) parent.ValueChanged(this, oldValue);
        }
    }
}
