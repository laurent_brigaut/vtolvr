﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    public interface IVRActor
    {
        string name { get; }
        object value { get; }
        IVRComponent parent { get; }


        void setValue(float v);
        void setValue(Vector3 v);

        void setParent(IVRComponent p);
    }
}
