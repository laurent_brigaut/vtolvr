﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.DirectInput;
using UnityEngine;

namespace VTOLHOTASDriver
{
    class SharpDXStick : IPhysicalController
    {
        // static properties
        private static DirectInput DiInstance = new DirectInput();
        private static List<DeviceInstance> DiDeviceInstances = null;
        
        private Dictionary<string, ActorsMatch> _matchedActors = new Dictionary<string, ActorsMatch>();

        private Joystick Device;

        public SharpDXStick(string v, ILogger l)
        {
            // load devices list singleton if necessary
            if (DiDeviceInstances == null) DiDeviceInstances = (List<DeviceInstance>)DiInstance.GetDevices();

            LoadStick(v);
            logger = l;

        }

        private void LoadStick(string v)
        {
            isLoaded = false;

            if (DiDeviceInstances != null)
            {
                foreach (var device in DiDeviceInstances)
                {
                    if (v.Equals(device.ProductName))
                    {
                        isLoaded = true;
                        name = device.ProductName;
                        Device = new Joystick(DiInstance, device.InstanceGuid);
                        Device.Properties.BufferSize = 128;
                        Device.Acquire();

                    }
                }
            }
                
        }


        public void matchActors(IPhysicalActor physical, IVRActor vr)
        {
            if (physical != null)
            {
                _matchedActors.Add(physical.name, new ActorsMatch(physical, vr));
            }
            
        }


        public void updateActors()
        {
            if (isLoaded == true)
            {
                var data = Device.GetBufferedData();

                foreach (var state in data)
                {
                    ActorsMatch actor;

                    if (_matchedActors.TryGetValue(state.Offset.ToString(), out actor ))
                    {
                     
                        if (actor.physicalActor != null) 
                        {
                            actor.physicalActor.value = state.Value;
                        }
                        if (actor.vrActor != null)
                        {
                            object v = actor.physicalActor.ValueToVR();
                            if (v is float)
                                actor.vrActor.setValue((float)v);
                            else if (v is Vector3)
                                actor.vrActor.setValue((Vector3)v);

                        }
                    }
                }
            }
        }

        public ArrayList GetMatchedActors()
        {
            ArrayList list = new ArrayList();

            foreach (var matchedActor in _matchedActors)
            {
                list.Add(matchedActor.Value);
            }

            return list;
        }

        private void Log(object message, LogLevel level = LogLevel.info, LogDestination dest = LogDestination.log_file)
        {
            if (logger != null)
            {
                logger._Log(message, level, dest);
            }
        }


        public string name { get; private set; }

        public bool isLoaded { get; private set; }
        public ILogger logger { get; private set;}
    }
}
