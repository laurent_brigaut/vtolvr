﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTOLHOTASDriver
{
    public enum LogLevel
    {
        none,
        info,
        debug
    }

    public enum LogDestination
    {
        log_file,
        flight_log
    }

    public interface ILogger
    {
        void _Log(object message, LogLevel level = LogLevel.info, LogDestination dest = LogDestination.log_file);
    }
}
