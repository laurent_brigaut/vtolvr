﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    public class ActorsMatch
    {
        public IPhysicalActor physicalActor { get; private set; }
        public IVRActor vrActor { get; private set; }

        public string paName 
        {
            get { if (physicalActor != null) return physicalActor.name; else return ""; }
        }

        public string paType
        {
            get { if (physicalActor != null) return physicalActor.type.ToString(); else return ""; }
        }

        public int paValue
        {
            get { if (physicalActor != null) return physicalActor.value; else return 0; }
        }

        public string vaName
        {
            get { if (vrActor != null) return vrActor.name; else return "No VR Actor"; }
        }

        public string vaValue
        {
            get 
            { 
                if (vrActor != null) 
                {
                    if (vrActor.value is Vector3)
                        return $"Vector3({((Vector3)vrActor.value).x},{((Vector3)vrActor.value).y},{((Vector3)vrActor.value).z})";
                    else
                        return vrActor.value.ToString();
                }  
                else return "No VR Actor"; 
            }
        }

        public ActorsMatch(IPhysicalActor physical, IVRActor vr) 
        {
            physicalActor = physical;
            vrActor = vr;
        }

    }
}
