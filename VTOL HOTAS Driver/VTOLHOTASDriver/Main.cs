using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.SceneManagement;


/*
 * V1.0.0 Known Issues :
 * - BUG : stick animation is flashing
 * - BUG : AH94 synchronize front collectives/thottle/pover with rear ones
 * - Check freeze when switchin battery (or any component) on the first time - guessing coming from FindVrComponent to CPU intensive!
 * 
 * TODO :
 * - implanter physical button to axis VR
 * - implanter support pour bouton "modifier" (Mode button)
 * - look at refactoring VR Components to reduce number of classes VRxxx
 * - ajout support d'autres controls VR (MFDs, list others ....)
 * - ajout support AutoPilot targets knobs
 * - document code and logic/architecture in order to release the code as opensource
 */
namespace VTOLHOTASDriver
{
    public class Main : VTOLMOD, ILogger
    {
        private enum LoadingState
        {
            not_started,
            in_progress,
            completed
        };

        private string[] dependencies = new string[] { "SharpDX.dll", "SharpDX.DirectInput.dll" };
        private List<IPhysicalController> _physicalControllers = new List<IPhysicalController>();
        private List<IVRComponent> _vrComponents = new List<IVRComponent>();
        private ILogger _logger = null;
        private bool _waitingForVrComponents = false;
        private bool _inCockpit = false;
        private LoadingState _loadingConfiguration = LoadingState.not_started;
        private String _modName = "VTOL HOTAS Driver";
        private LogLevel _logLevel = LogLevel.info;
        private LogDestination _logDestination = LogDestination.log_file;

              
        public override void ModLoaded()
        {

            //This is an event the VTOLAPI calls when the game is done loading a scene
            DontDestroyOnLoad(this.gameObject);
            VTOLAPI.SceneLoaded += SceneLoaded;
            VTOLAPI.MissionReloaded += MissionReloaded;

            Initialize();

            base.ModLoaded();

            _inCockpit = false;
            _waitingForVrComponents = false;

            _Log($"Mod {_modName} loaded!",LogLevel.debug);
        }


        public void Initialize(ILogger logger = null)
        {
            _logger = logger;

            // Loading dependencies
            Assembly assembly;
            foreach (string d in dependencies) 
            {
                assembly = Assembly.Load(File.ReadAllBytes(Path.Combine(this.ModFolder, "Assets", d).ToString()));
            }
        }

        public ArrayList GetPhysicalcontrollers()
        {
            return new ArrayList(_physicalControllers);
        }

        private IEnumerator LoadConfiguration()
        {
            String settingsFile = "";

            _Log($"ModFolder3 : {this.ModFolder}");

            _Log($"Start loading configuration file", LogLevel.debug);

            _loadingConfiguration = LoadingState.in_progress;

            // Looking for the installation path
            _Log($"Looking for configuration file path", LogLevel.debug);
            List<Mod> mods = VTOLAPI.GetUsersMods();
            for (int i = 0; i < mods.Count; i++)
            {
                if (mods[i].name.Equals(_modName))
                {
                    settingsFile = Path.Combine(Path.GetDirectoryName(mods[i].dllPath), "settings.xml");
                    _Log($"Checking for configuration file path {settingsFile} ...", LogLevel.debug);
                    if (!File.Exists(settingsFile))
                    {
                        settingsFile = Path.Combine(mods[i].ModFolder, "settings.xml");
                        _Log($"... Not found - Checking for alternative configuration file path {settingsFile}", LogLevel.debug);
                        if (!File.Exists(settingsFile))
                        {
                            _Log($"Could not find Settings.xml ....", LogLevel.debug);
                            settingsFile = "";
                        }
                    }
                }
                
            }
            yield return null;

            if (settingsFile != "")
            {
                _Log($"Reading settings file : {settingsFile}");

                var deserializer = new XmlSerializer(typeof(Settings));
                TextReader reader = new StreamReader(settingsFile);

                yield return null;
                
                Settings settings = (Settings)deserializer.Deserialize(reader);
                reader.Close();

                yield return null;

                // Handling Log level and destination
                if (settings.Log != null)
                {
                    if (settings.Log.level != null) 
                    {
                        LogLevel lv;
                        if (Enum.TryParse<LogLevel>(settings.Log.level, true, out lv))
                            _logLevel = lv;
                    }

                    if (settings.Log.destination != null)
                    {
                        LogDestination ld;
                        if (Enum.TryParse<LogDestination>(settings.Log.destination, true, out ld))
                            _logDestination = ld;
                    }

                    _Log($"Log Level is set to {_logLevel} and destination is set to {_logDestination}", LogLevel.debug);

                }

                // Handling Devices and matches
                Dictionary<string, IVRComponent> vr_components = new Dictionary<string, IVRComponent>();

                if (settings.Mappings != null)
                {
                    _Log($"Found {settings.Mappings.Length} devices in configuration file", LogLevel.debug);
                    foreach (var device in settings.Mappings)
                    {
                        _Log($"Configuring {device.name} - {device.@class} device", LogLevel.debug);
                        System.Object[] args = new System.Object[2];
                        args[0] = device.name;
                        args[1] = this;

                        Type ctrlObjType = Type.GetType($"VTOLHOTASDriver.{device.@class}");

                        if (ctrlObjType != null)
                        {
                            IPhysicalController controller = (IPhysicalController)Activator.CreateInstance(ctrlObjType, args);

                            if (controller != null)
                            {
                                if (controller.isLoaded)
                                {
                                    _physicalControllers.Add(controller);
                                    _Log($"{controller.name} device loaded!", LogLevel.debug);

                                    if (device.Match != null)
                                    {
                                        _Log($"Found {device.Match.Length} matches for device {device.name}", LogLevel.debug);
                                        foreach (var match in device.Match)
                                        {
                                            _Log("Matching ...", LogLevel.debug);
                                            _Log($"Device actor class : {match.DeviceActor.@class}", LogLevel.debug);
                                            _Log($"Device actor name : {match.DeviceActor.name}", LogLevel.debug);
                                            _Log($"Device actor inverted : {match.DeviceActor.invert}", LogLevel.debug);
                                            _Log("... With ...", LogLevel.debug);
                                            _Log($"VR class : {match.VRComponent.@class}", LogLevel.debug);
                                            _Log($"VR name : {match.VRComponent.name}", LogLevel.debug);
                                            _Log($"VR action : {match.VRComponent.action}", LogLevel.debug);
                                            _Log($"VR actor : {match.VRComponent.actor}", LogLevel.debug);

                                            System.Object[] actor_args;
                                            System.Object[] vr_args;

                                            if (match.DeviceActor.invertSpecified)
                                            {
                                                actor_args = new System.Object[2];
                                                actor_args[0] = match.DeviceActor.name;
                                                actor_args[1] = match.DeviceActor.invert;
                                            }
                                            else
                                            {
                                                actor_args = new System.Object[1];
                                                actor_args[0] = match.DeviceActor.name;
                                            }

                                            if (match.VRComponent.action != null)
                                            {
                                                vr_args = new System.Object[3];
                                                vr_args[0] = match.VRComponent.name;
                                                vr_args[1] = match.VRComponent.action;
                                                vr_args[2] = this;
                                            }
                                            else
                                            {
                                                vr_args = new System.Object[2];
                                                vr_args[0] = match.VRComponent.name;
                                                vr_args[1] = this;
                                            }

                                            yield return null;

                                            IVRComponent vr_component;

                                            StringBuilder componentID = new StringBuilder(255);
                                            componentID.Append($"{match.VRComponent.@class}_{match.VRComponent.name}");
                                            if (match.VRComponent.action != null)
                                                componentID.Append($"_{match.VRComponent.action}");

                                            vr_components.TryGetValue(componentID.ToString(), out vr_component);
                                            yield return null;

                                            if (vr_component == null)
                                            {
                                                Type cmpObjType = Type.GetType($"VTOLHOTASDriver.VR{match.VRComponent.@class}");

                                                if (cmpObjType != null)
                                                {
                                                    vr_component = (IVRComponent)Activator.CreateInstance(cmpObjType, vr_args);

                                                    if (vr_component != null)
                                                    {
                                                        vr_components.Add(componentID.ToString(), vr_component);
                                                        _vrComponents.Add(vr_component);
                                                    }
                                                    else _Log($"Warning : Could not instantiate {match.VRComponent.@class} VRComponent - check attributes!");
                                                }
                                                else _Log($"Warning : {match.VRComponent.@class} - Unknown VRComponent class {controller.name}!");

                                            }

                                            yield return null;

                                            if (vr_component != null)
                                            {
                                                String propName;
                                                if (match.VRComponent.actor != null)
                                                    propName = match.VRComponent.actor;
                                                else
                                                    propName = match.VRComponent.@class;

                                                var property = Type.GetType($"VTOLHOTASDriver.VR{match.VRComponent.@class}").GetProperty(propName,
                                                                        BindingFlags.Public
                                                                        | BindingFlags.Instance
                                                                        | BindingFlags.IgnoreCase);

                                                yield return null;

                                                if (property != null)
                                                {
                                                    Type actorObjType = Type.GetType($"VTOLHOTASDriver.{match.DeviceActor.@class}Actor");

                                                    if (actorObjType != null)
                                                    {
                                                        IPhysicalActor pa = (IPhysicalActor)Activator.CreateInstance(actorObjType, actor_args);

                                                        if (pa != null)
                                                            controller.matchActors(pa, (IVRActor)property.GetValue(vr_component));
                                                        else _Log($"Warning : Could not instantiate {match.DeviceActor.@class}Actor - check attributes!");

                                                    }
                                                    else _Log($"Warning : Invalid DeviceActor class - {match.DeviceActor.@class} does not exist!");
                                                }
                                                else _Log($"Warning : {propName} unknown actor attribute!");

                                                yield return null;
                                            }
                                        }
                                    }
                                    else _Log($"Warning : No <Match> section for {controller.name} device!");
                                }
                                else _Log($"Warning : Can't find {device.name} device!");
                            }
                            else _Log($"Warning : Could not instantiate {device.@class}!");
                        }
                        else _Log($"Warning : Invalid device class - {device.@class} does not exist!");

                        yield return null;

                    }
                }
                else _Log($"Warning : Invalid configuration file - Missing <Mappings> section!");
            }
            else _Log($"Warning : Configuration file not found!");

            _loadingConfiguration = LoadingState.completed;

            _Log($"Loading configuration file completed", LogLevel.debug);
        }

       
        void Update()
        {
            if (_inCockpit)
            {
                if (!_waitingForVrComponents)
                {
                    UpdateSticks();
                    UpdateVRComponent();
                }
            }

        }


        private void UpdateVRComponent()
        {
            VRBaseComponent.UpdateComponents();
        }

        private void UpdateSticks()
        {
            foreach (var stick in _physicalControllers)
            {
               stick.updateActors();
            }
        }

        //This method is like update but it's framerate independent. This means it gets called at a set time interval instead of every frame. This is useful for physics calculations
        void FixedUpdate()
        {

        }

        //This function is called every time a scene is loaded. this behaviour is defined in Awake().
        private void SceneLoaded(VTOLScenes scene)
        {
            _Log($"Scene Loaded : {scene.ToString()} - Index : {SceneManager.GetActiveScene().buildIndex}", LogLevel.debug);
            //If you want something to happen in only one (or more) scenes, this is where you define it.

            // First time this is called, we start loading the configuration
            if (_loadingConfiguration == LoadingState.not_started)
            {
                _Log("Calling Coroutine to load configuration", LogLevel.debug);
                StartCoroutine(LoadConfiguration());
            }

            //For example, lets say you're making a mod which only does something in the ready room and the loading scene. This is how your code could look:
            switch (scene)
            {
                case VTOLScenes.Akutan:
                case VTOLScenes.CustomMapBase:

                    _Log($"Entered cockpit of {VTOLAPI.GetPlayersVehicleEnum().ToString()}", LogLevel.debug);

                    _inCockpit = true;
                    _waitingForVrComponents = true;

                    _Log("Calling Coroutine to catch VR components to be matched with physical components", LogLevel.debug);
                    StartCoroutine(FindVRComponents());
                    break;

                default:
                    if (_inCockpit)
                    {
                        _Log("Left cockpit of {VTOLAPI.GetPlayersVehicleEnum().ToString()}", LogLevel.debug);

                        _inCockpit = false;
                        _waitingForVrComponents = false;
                        VRBaseComponent.CleanUp();
                    }
                        
                     break;
              

            }
        }

        //This function is called every time a mission is reloaded.
        private void MissionReloaded()
        {
            _Log("Mission Reloaded!", LogLevel.debug);
            VRBaseComponent.CleanUp();
            _waitingForVrComponents = true;

            _Log("Calling Coroutine to catch VR components to be matched with physical components", LogLevel.debug);
            StartCoroutine(FindVRComponents());
        }

        public void _Log(object message, LogLevel level = LogLevel.info, LogDestination dest = LogDestination.log_file)
        {
            if (_logLevel >= level)
            {
                if (message != null)
                {
                    if (_logger != null)
                        _logger._Log($"{DateTime.UtcNow.ToString("o")} : {message}", level, dest);
                    else
                    {

                        if (_logDestination == LogDestination.log_file)
                            Log($"{DateTime.UtcNow.ToString("o")} : {message}");
                        else
                            FlightLogger.Log(message.ToString());
                    }
                }
            }
        }


        private IEnumerator FindVRComponents()
        {
            _Log($"Start FindVRComponents", LogLevel.debug);

            while (_loadingConfiguration != LoadingState.completed)
            {
                // make sure configuration is done loading
                yield return new WaitForSeconds((float)0.1);
            }

            _Log($"Initializing for {_vrComponents.Count} VR Components", LogLevel.debug);
            foreach (IVRComponent component in _vrComponents)
            {
                yield return null;
                _Log($"Initializing for {component.name} Component", LogLevel.debug);
                component.Initialize();
                if (component.device != null)
                    _Log($"{component.name} Component Initialized!", LogLevel.debug);
                else
                    _Log($"Warning : Could not initialize {component.name} Component!");
            }

            _waitingForVrComponents = false;

            _Log($"FindVRComponents : Completed", LogLevel.debug);
        }
    }
}