﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTOLHOTASDriver
{
    public enum VRThrottleId
    {
        // All planes
        throttle,

        // AH94
        combatControls,
        power,
        frontPower
    }

    class VRThrottleCore
    {
        static private Dictionary<String, String> _throttlesNames = new Dictionary<String, String>
        {
            {"throttle", "throttleInteractable"},
            {"throttle_AH94", "flightCollectiveInteractable_rear"},
            {"frontThrottle_AH94", "flightCollectiveInteractable_front"},
            {"combatControls_AH94", "combatCollectiveInteractable_rear"},
            {"power_AH94", "powerInteractable_rear"},
            {"frontPower_AH94", "powerInteractable_front"}
        };

        static public bool FindDeviceNameById(string id, out string deviceName)
        {
            bool deviceFound = true;

            if (!_throttlesNames.TryGetValue($"{id}_{VTOLAPI.GetPlayersVehicleEnum().ToString()}", out deviceName))
                if (!_throttlesNames.TryGetValue(id, out deviceName))
                    deviceFound = false;

            return deviceFound;
        }
    }
}
