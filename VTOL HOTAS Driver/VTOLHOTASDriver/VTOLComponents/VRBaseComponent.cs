﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    class VRBaseComponent : IVRComponent
    {
        static List<IVRComponent> _dirtyComponents = new List<IVRComponent>();
        static Dictionary<string, MonoBehaviour> _vrDevices = new Dictionary<string, MonoBehaviour>();

        public string name { get; set; }

        public MonoBehaviour device { get; protected set; }

        private ILogger _logger = null;

        private Dictionary<string, IVRActor> _actors = new Dictionary<string, IVRActor>();

        static public void CleanUp()
        {
            _dirtyComponents.Clear();
            _vrDevices.Clear();
        }

        static protected void AppendVRDevice(String name, MonoBehaviour device)
        {
            _vrDevices.Add(name, device);
        }

        static protected MonoBehaviour GetVRDeviceByName(string name)
        {
            MonoBehaviour device = null;
            _vrDevices.TryGetValue(name, out device);
            return device;
        }

        static public void UpdateComponents()
        {
            List<IVRComponent> tmpList = new List<IVRComponent>(_dirtyComponents);

            foreach (IVRComponent component in tmpList)
            {
                component.Update();
            }
        }


        static protected void AppendDirtyComponent(IVRComponent component)
        {
            _dirtyComponents.Add(component);
        }

        static protected bool IsDirtyComponent(IVRComponent component)
        {
            return (_dirtyComponents.IndexOf(component) >= 0);
        }

        static protected void RemoveDirtyComponent(IVRComponent component)
        {
            _dirtyComponents.Remove(component);
        }

        public VRBaseComponent(String n, ILogger logger = null)
        {
            _logger = logger;
            name = n;
        }

        public virtual void Initialize()
        {
            device = null;
        }
        public virtual void Update()
        {
            RemoveDirtyComponent(this);
        }

        public virtual void ValueChanged(IVRActor sender, object oldValue)
        {
            if (!IsDirtyComponent(this)) AppendDirtyComponent(this);
        }

        protected void Log(object message, LogLevel level = LogLevel.info, LogDestination dest = LogDestination.log_file)
        {
            if (_logger != null) _logger._Log(message, level, dest);
        }

        public void AddActor(IVRActor actor)
        {
            _actors.Add(actor.name, actor);
        }

        public IVRActor GetActorByName(string name)
        {
            IVRActor actor = null;

            _actors.TryGetValue(name, out actor);  

            return actor;
        }

        protected MemberInfo GetMemberInfo(string memberName)
        {
            MemberInfo result = null;

            Log($"GetMemberInfo for {memberName}");
            if (device != null)
            {
                Type objType = device.GetType();

                Log($"GetMemberInfo for {memberName} - Object Type : {objType.ToString()}");
                if (objType != null)
                {
                    MemberInfo[] memberInfo = objType.GetMember(memberName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);

                    if (memberInfo != null)
                    {
                        Log($"GetMemberInfo for {memberName} - Info found : {memberInfo.Length}");
                        result = memberInfo[0];
                    }
                        
                }
                
            }
            
            return result;
        }
    }
}
