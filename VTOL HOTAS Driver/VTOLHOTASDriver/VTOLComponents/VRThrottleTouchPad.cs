﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    class VRThrottleTouchPad : VRBaseComponent
    {
        public VRThrottleTouchPad(String name, ILogger logger = null) : base(name, logger)
        {
            BaseVRActor actor = new BaseVRActor("Up");
            actor.setParent(this);
            AddActor(actor);

            actor = new BaseVRActor("Down");
            actor.setParent(this);
            AddActor(actor);

            actor = new BaseVRActor("Right");
            actor.setParent(this);
            AddActor(actor);

            actor = new BaseVRActor("Left");
            actor.setParent(this);
            AddActor(actor);
        }

        public override void Initialize()
        {
            Log($"Initializing VRThrottle {name}", LogLevel.debug);

            String deviceName;
            bool componentFound = VRThrottleCore.FindDeviceNameById(name, out deviceName);

            if (componentFound)
            {
                Log($"VRThrottle - Found {deviceName} for {name}", LogLevel.debug);

                device = GetVRDeviceByName(deviceName);
                if (device == null)
                {
                    Log($"VRThrottle {deviceName} not initialized yet ....", LogLevel.debug);
                    MonoBehaviour[] devices = (MonoBehaviour[])MonoBehaviour.FindObjectsOfType<VRThrottle>();

                    if (devices != null && devices.Length > 0)
                    {
                        // Identify the instance based on the instance ID provided (InstanceID)
                        device = Array.Find(devices, s => s.name.Equals(deviceName));

                        if (device != null)
                        {
                            Log($"VRThrottle {deviceName} initialized and added to the list : {device.name}", LogLevel.debug);
                            AppendVRDevice(deviceName, device);
                        }
                        else Log($"Warning : could not initialize VRThrottle {deviceName}!");
                    }
                }
                else Log($"... VRThrottle {deviceName} already initialized!", LogLevel.debug);
            }
            else Log($"Can't find VRThrottle named {name} in the supported dictonary");
        }

        public BaseVRActor Up
        {
            get { return (BaseVRActor)GetActorByName("Up"); }
        }

        public BaseVRActor Down
        {
            get { return (BaseVRActor)GetActorByName("Down"); }
        }

        public BaseVRActor Left
        {
            get { return (BaseVRActor)GetActorByName("Left"); }
        }

        public BaseVRActor Right
        {
            get { return (BaseVRActor)GetActorByName("Right"); }
        }

        public override void Update()
        {

            if (device != null)
            {
                Vector3 v = new Vector3(0, 0, 0);

                if ((Up != null) && ((float)Up.value > 0)) v.y = 1;
                else if ((Down != null) && ((float)Down.value > 0)) v.y = -1;

                if ((Right != null) && ((float)Right.value > 0)) v.x = 1;
                else if ((Left != null) && ((float)Left.value > 0)) v.x = -1;

                

                if (v.Equals(new Vector3(0, 0, 0)))
                {
                    ((VRThrottle)device).OnResetThumbstick.Invoke();
                    base.Update();
                }  
                else ((VRThrottle)device).OnSetThumbstick.Invoke(v);

            }
        }
    }
}
