﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    public enum VRHelmetAction
    {
        toggleDisplay,
        toggleVisor,
        toggleNvg,
        noAction
    }

    class VRHelmet : VRBaseComponent
    {
        public VRHelmetAction action { get; set; }

        public VRHelmet(String name, String hAction, ILogger logger = null) : base(name + hAction, logger)
        {
            VRHelmetAction a;
            if (Enum.TryParse<VRHelmetAction>(hAction, true, out a))
                action = a;
            else
            {
                action = VRHelmetAction.noAction;
                this.name = name + action.ToString();
            }

            BaseVRActor actor = new BaseVRActor("Helmet");
            actor.setParent(this);
            AddActor(actor);
        }

        public BaseVRActor Helmet
        {
            get { return (BaseVRActor)GetActorByName("Helmet"); }
        }

        public override void Initialize()
        {
            Log("Initializing HelmetController...", LogLevel.debug);
            device = GetVRDeviceByName("helmetController");

            if (device == null)
            {
                Log("HelmetController not initialized yet ....", LogLevel.debug);
                device = MonoBehaviour.FindObjectOfType<HelmetController>();

                if (device != null)
                {
                    Log($"HelmetController initialized and added to the list : {device.name}", LogLevel.debug);
                    AppendVRDevice("helmetController", device);
                }
                else Log("Warning : could not initialize HelmetController!");

            }
            else Log("... HelmetController already initialized!", LogLevel.debug);
        }

        public override void Update()
        {
            if ((device != null) && (Helmet.value is float))
            {
                switch (action)
                {
                    case VRHelmetAction.noAction:
                        break;
                    case VRHelmetAction.toggleDisplay:
                        ((HelmetController)device).ToggleDisplay();
                        break;
                    case VRHelmetAction.toggleNvg:
                        ((HelmetController)device).ToggleNVG();
                        break;
                    case VRHelmetAction.toggleVisor:
                        ((HelmetController)device).ToggleVisor();
                        break;
                }
            }

            base.Update();
        }

        public override void ValueChanged(IVRActor sender, object oldValue)
        {
            if ((sender.value != oldValue) && ((float)sender.value > 0))
                if (!IsDirtyComponent(this))
                    AppendDirtyComponent(this);
        }
    }
}
