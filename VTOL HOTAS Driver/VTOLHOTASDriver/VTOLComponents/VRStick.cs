﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    class VRStick : VRContinuousComponent
    {
        private MethodInfo miRemoteSetStick;
        public VRStick(String name, ILogger logger = null) : base(name, logger)
        {
            BaseVRActor actor = new BaseVRActor("Pitch");
            actor.setParent(this);
            AddActor(actor);

            actor = new BaseVRActor("Roll");
            actor.setParent(this);
            AddActor(actor);

            actor = new BaseVRActor("Yaw");
            actor.setParent(this);
            AddActor(actor);
        }

        public override void Initialize()
        {
            Log("Initializing VRStick...", LogLevel.debug);
            device = GetVRDeviceByName("joyInteractable");

            if (device == null)
            {
                Log("VRStick not initialized yet ....", LogLevel.debug);
                device = MonoBehaviour.FindObjectOfType<VRJoystick>();

                if (device != null)
                {
                    Log($"VRStick initialized and added to the list : {device.name}", LogLevel.debug);
                    AppendVRDevice("joyInteractable", device);
                }
                else Log("Warning : could not initialize VRStick!");

            }
            else Log("... VRStick already initialized!", LogLevel.debug);

            miRemoteSetStick = (MethodInfo)GetMemberInfo("RemoteSetStick");
        }

        public BaseVRActor Pitch
        {
            get { return (BaseVRActor)GetActorByName("Pitch"); }
        }

        public BaseVRActor Roll
        {
            get { return (BaseVRActor)GetActorByName("Roll"); }
        }

        public BaseVRActor Yaw
        {
            get { return (BaseVRActor)GetActorByName("Yaw"); }
        }

        public override void Update()
        {
            if (device != null)
            {
                RemoteSetStick(new Vector3((float)Pitch.value, (float)Yaw.value, (float)Roll.value));
                ((VRJoystick)device).OnSetStick.Invoke(new Vector3((float)Pitch.value, (float)Yaw.value, (float)Roll.value));
            }

            base.Update();
        }
      
        private void RemoteSetStick(Vector3 value)
        {
            if ((device != null) && (miRemoteSetStick != null))
            {
                List<object> argObjs = new List<object>();
                argObjs.Add(value);

                miRemoteSetStick.Invoke(device, argObjs.ToArray());
            }
        }
    }

}
