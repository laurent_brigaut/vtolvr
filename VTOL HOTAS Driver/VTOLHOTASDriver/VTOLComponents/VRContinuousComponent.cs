﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTOLHOTASDriver
{
    class VRContinuousComponent : VRBaseComponent
    {
        // Component that never gets removed from dirty list to be updated every frame even if not changed

        public VRContinuousComponent(String name, ILogger logger = null) : base(name, logger)
        {   
        }

        public override void Update()
        {
        }

        public override void ValueChanged(IVRActor sender, object oldValue)
        {
            // we add the component to the dirty list only the first time it's value gets changed (it will remain on the list!)
            if (!IsDirtyComponent(this))
            {
                AppendDirtyComponent(this);
            }
        }
    }
}
