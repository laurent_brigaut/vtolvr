﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    public enum VRKnobId
    {
        // All Planes
        LabelBrightness,
        RadioVolume,
        CommsVolume,
        MFDBrightness,
        HUDBrightness,
        HUDTint,

        // AH-94
        IntraCommsVolume,
        HMDBrightness
    }

    class VRKnob : VRContinuousComponent
    {

        static private Dictionary<String, String> _knobNames = new Dictionary<String, String>
        {
            {"LabelBrightness","LabelBrightnessKnob"},
            {"LabelBrightness_AH94","LabelBrightnessKnob_rear"},
            {"RadioVolume","RadioVolumeKnob"},
            {"CommsVolume","CommsVolumeKnob"},
            {"CommsVolume_AH94","CommsVolumeKnob"},
            {"MFDBrightness","MFDBrightnessKnob"},
            {"HUDBrightness","HUDBrightKnob"},
            {"HUDTint","HUDTintKnob"},
            {"IntraCommsVolume_AH94", "IntraCommsVolumeKnob_rear"},
            {"HMDBrightness_AH94", "HMDBrightKnob_rear"}
        };

        public String knob_name { get; }

        public VRKnob(String name, ILogger logger = null) : base(name, logger)
        {
            knob_name = name;

            BaseVRActor actor = new BaseVRActor("Knob");
            actor.setParent(this);
            AddActor(actor);
        }

        public BaseVRActor Knob
        {
            get { return (BaseVRActor)GetActorByName("Knob"); }
        }

        public override void Initialize()
        {
            Log($"Initializing VRKnob {knob_name}", LogLevel.debug);

            String deviceName;

            bool componentFound = true;

            if (!_knobNames.TryGetValue($"{knob_name}_{VTOLAPI.GetPlayersVehicleEnum().ToString()}", out deviceName))
                if (!_knobNames.TryGetValue(knob_name, out deviceName))
                    componentFound = false;

            if (componentFound)
            {
                Log($"VRKnob - Found {deviceName} for {knob_name}", LogLevel.debug);
                
                device = GetVRDeviceByName(knob_name);
                if (device == null)
                {
                    Log($"VRKnob {knob_name} not initialized yet ....", LogLevel.debug);
                    MonoBehaviour[] devices = (MonoBehaviour[])MonoBehaviour.FindObjectsOfType<VRTwistKnob>();

                    if (devices != null && devices.Length > 0)
                    {
                        // Identify the instance based on the instance ID provided (InstanceID)
                        device = Array.Find(devices, s => s.name.Equals(deviceName));

                        if (device != null)
                        {
                            Log($"VRKnob {knob_name} initialized and added to the list : {device.name}", LogLevel.debug);
                            AppendVRDevice(knob_name, device);
                        }
                        else Log($"Warning: could not initialize VRKnob {knob_name}!");
                    }
                }
                else Log($"... VRKnob {knob_name} already acquired!", LogLevel.debug);
            }
            else Log($"Can't find VRKnob named {knob_name} in the supported dictonary");
        }

        public override void Update()
        {
            if ((device != null)  && (Knob.value is float))
            {
                ((VRTwistKnob)device).SetKnobValue((float)Knob.value);
                base.Update();
            }

            
        }
        
    }
}
