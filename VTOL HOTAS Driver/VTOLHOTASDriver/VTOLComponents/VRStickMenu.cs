﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    class VRStickMenu : VRPushButton
    {

        public VRStickMenu(String name, ILogger logger = null) : base(name, logger)
        {
            BaseVRActor actor = new BaseVRActor("StickMenu");
            actor.setParent(this);
            AddActor(actor);
        }

        public BaseVRActor StickMenu
        {
            get { return (BaseVRActor)GetActorByName("StickMenu"); }
        }

        public override void Initialize()
        {
            Log("Initializing VRStick...", LogLevel.debug);
            device = GetVRDeviceByName("joyInteractable");

            if (device == null)
            {
                Log("VRStick not initialized yet ....", LogLevel.debug);
                device = MonoBehaviour.FindObjectOfType<VRJoystick>();

                if (device != null)
                {
                    Log($"VRStick initialized and added to the list : {device.name}", LogLevel.debug);
                    AppendVRDevice("joyInteractable", device);
                }
                else Log("Warning : could not initialize VRStick!");

            }
            else Log("... VRStick already initialized!", LogLevel.debug);
        }


        public override void Update()
        {
            if ((device != null) && (StickMenu.value is float))
            {
                if ((float)StickMenu.value > 0)
                    ((VRJoystick)device).OnMenuButtonDown.Invoke();
                else
                    ((VRJoystick)device).OnMenuButtonUp.Invoke();
            }

            base.Update();
        }
    }
}
