﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    public enum VRSwitchAction
    {
        on,
        off,
        toggle,
        up,
        down
    }

    public enum VRSwitchId
    {
        // All Planes
        LandingLight,
        Strobe,
        NavLight,
        InstrumentLight,
        BayLights,
        CockpitLights,
        FuelPort,
        GLimit,
        PitchAutoTrim,
        RollSAS,
        YawSAS,
        PitchSAS,
        AssistMaster,
        Flare,
        Chaff,
        BrakeLock,
        Ramp,
        CoverFuelDump,
        FuelDump,
        MainBattery,
        CoverJettison,
        CoverMasterArm,
        MasterArm,
        CatHook,
        Hook,
        Gear,
        Apu,
        CoverRightEngine,
        RightEngine,
        CoverLeftEngine,
        LeftEngine,
        HmcsPower,
        HudPower,
        Flaps,
        RWRMode,
        RCSMode,
        RadioChannel,
        Radio,
        MFD3,
        MFD2,
        MFD1,
        HUDDeclutter,

        // F/A-26B
        RadarPower,
        Canopy,
        Wing,
        CATOTrim,

        // F45A
        FormationLight,
        Jettison,

        // AH94
        InstrumentLightFront, 
        RotorBrake, 
        RotorFold,
        MFDGunner3,
        MFDGunner1,
        MFDGunner2,
        FrontRWRPower,
        IntraComm,
        ADIPower,
        RearRWRPower
    }


    class VRSwitch : VRBaseComponent
    {
        static private Dictionary<String, String> _switchNames = new Dictionary<String, String>
        {
            {"LandingLight_AV42C","LandingLightSwitchInteractable VRLever"},
            {"LandingLight_AH94","LandingLightInteractable_rear VRLever"},
            {"LandingLight","LandingLightInteractable VRLever"},
            {"Strobe_AV42C","StrobeSwitchInteractable VRLever"},
            {"Strobe_AH94","StrobLightInteractable_rear VRLever"},
            {"Strobe","StrobLightInteractable VRLever"},
            {"NavLight_AV42C","NavLightInteractable_rear VRLever"},
            {"NavLight_AH94","NavLightSwitchInteractable VRLever"},
            {"NavLight","NavLightInteractable VRLever"},
            {"InstrumentLight_AV42C","LabelGlowPowerInteractable VRLever"},
            {"InstrumentLight_AH94","InstrumentLightInteractable_rear VRLever"},
            {"BayLights","BayLightsInteractable VRLever"},
            {"CockpitLights","CockpitLightsInteractable VRLever"},
            {"CockpitLights_FA26B","InteriorLightInteractable VRLever"},
            {"CockpitLights_AH94","InteriorLightInteractable_rear VRLever"},
            {"FuelPort","FuelPort VRLever"},
            {"GLimit","GLimitSwitchInteractable VRLever"},
            {"GLimit_FA26B","GLimitInteractable VRLever"},
            {"PitchAutoTrim","PitchAutoTrimInteractable VRLever"},
            {"RollSAS","RollSASInteractable VRLever"},
            {"YawSAS","YawSASInteractable VRLever"},
            {"PitchSAS","PitchSASInteractable VRLever"},
            {"AssistMaster","AssistMasterInteractable VRLever"},
            {"Flare","FlareInteractable VRLever"},
            {"Chaff","ChaffInteractable VRLever"},
            {"BrakeLock","BrakeLockInteractable VRLever"},
            {"BrakeLock_F45A","parkingBrakeInteractable VRLever"},
            {"BrakeLock_AH94","parkingBrakeSwitchInteractable VRLever"},
            {"Ramp","RampInteractable VRLever"},
            {"CoverFuelDump","coverSwitchInteractable_fuelDump VRLever"},
            {"FuelDump","fuelDumpSwitchInteractable VRLever"},
            {"MainBattery","mainBattSwitchInteractable VRLever"},
            {"CoverJettison","coverSwitchInteractable_jettisonButton VRLever"},
            {"CoverMasterArm","coverSwitchInteractable_masterArm VRLever"},
            {"MasterArm","masterArmSwitchInteractable VRLever"},
            {"CatHook","CatHookInteractable VRLever"},
            {"Hook","HookInteractable VRLever"},
            {"Gear","GearInteractable VRLever"},
            {"Apu","apuSwitchInteractable VRLever"},
            {"CoverRightEngine","coverSwitchInteractable_rightEngine VRLever"},
            {"RightEngine","rightEngineSwitchInteractable VRLever"},
            {"RightEngine_AH94","starterSwitch2Interactable VRLever"},
            {"CoverLeftEngine","coverSwitchInteractable_leftEngine VRLever"},
            {"LeftEngine","leftEngineSwitchInteractable VRLever"},
            {"LeftEngine_F45A","engineSwitchInteractable VRLever"},
            {"LeftEngine_AH94","starterSwitch1Interactable VRLever"},
            {"HmcsPower","hmcsPowerInteractable VRLever"},
            {"HmcsPower_F45A","HMCSPowerInteractable VRLever"},
            {"HmcsPower_AH94","HMDPwr VRTwistKnobInt"},
            {"HudPower","hudPowerInteractable VRLever"},
            {"Flaps", "FlapsInteractable VRLever"},
            {"RWRMode","RWRModeInteractable VRTwistKnobInt"},
            {"RCSMode","RCSModeInteractable VRTwistKnobInt"},
            {"RadioChannel","RadioChannelInteractable VRTwistKnobInt"},
            {"RadioChannel_AH94","RadioChannelInteractable_rear VRTwistKnobInt"},
            {"Radio","RadioInteractable VRTwistKnobInt"},
            {"Radio_AH94","RadioInteractable_rear VRTwistKnobInt"},
            {"MFD3","MFD3PowerInteractable VRTwistKnobInt"},
            {"MFD2","MFD2PowerInteractable VRTwistKnobInt"},
            {"MFD1","MFD1PowerInteractable VRTwistKnobInt"},
            {"HUDDeclutter","HUDDeclutterKnob VRTwistKnobInt"},
            {"HUDDeclutter_FA26B","DeclutterKnob VRTwistKnobInt"},
            {"RadarPower_FA26B","RadarPowerInteractable VRTwistKnobInt"},
            {"Canopy","CanopyInteractable VRLever"},
            {"Wing","WingSwitchInteractable VRLever"},
            {"CATOTrim_FA26B","WingSwitchInteractable VRLever"},
            {"InstrumentLight","InsturmentLightInteractable VRLever"},
            {"FormationLight","FormationGlowInteractable VRLever"},
            {"Jettison_F45A","JettisonSwitchInteractable VRTwistKnobInt"},
            {"InstrumentLightFront","InteriorLightInteractable_front VRLever"},
            {"RotorBrake","rotorBrakeSwitchInteractable VRLever"},
            {"RotorFold","rotorFoldSwitchInteractable VRLever"},
            {"MFDGunner3_AH94", "MFDGunner3-PowerKnob VRTwistKnobInt"},
            {"MFDGunner1_AH94", "MFDGunner1-PowerKnob VRTwistKnobInt"},
            {"MFDGunner2_AH94", "MFDGunner2-PowerKnob VRTwistKnobInt"},
            {"FrontRWRPower_AH94", "FrontRWRPowerKnob VRTwistKnobInt"},
            {"IntraComm_AH94", "IntraCommInteractable_rear VRTwistKnobInt"},
            {"ADIPower_AH94", "RearADIPowerKnob VRTwistKnobInt"},
            {"RearRWRPower_AH94", "RearRWRPowerKnbo VRTwistKnobInt"}
        };

        public VRSwitchAction action { get; set; }
        public String switch_name { get; }
        private Type switch_type;

        // Hack to get AH94 3-states switch to work
        private int _3StatesSwitch_state = 0;


        public VRSwitch(String name, String swAction, ILogger logger = null) : base(name+ swAction, logger)
        {
            switch_name = name;   
            VRSwitchAction a;
            if (Enum.TryParse<VRSwitchAction>(swAction, true, out a))
                action = a;
            else
            {
                action = VRSwitchAction.toggle;
                this.name = name + action.ToString();
            }
            
            BaseVRActor actor = new BaseVRActor("Switch");
            actor.setParent(this);
            AddActor(actor);
        }

        public BaseVRActor Switch
        {
            get { return (BaseVRActor)GetActorByName("Switch"); }
        }

        public override void Initialize()
        {
            Log($"Initializing VRSwitch {switch_name}", LogLevel.debug);

            String deviceName;
            bool componentFound = true;

            if (!_switchNames.TryGetValue($"{switch_name}_{VTOLAPI.GetPlayersVehicleEnum().ToString()}", out deviceName))
                if (!_switchNames.TryGetValue(switch_name, out deviceName))
                    componentFound = false;

            if (componentFound)
            {
                Log($"VRSwitch - Found {deviceName} for {switch_name}", LogLevel.debug);
                String[] splittedDeviceName = deviceName.Split(' ');

                Log($"VRSwitch - Looking for {splittedDeviceName[0]}", LogLevel.debug);
                Log($"VRSwitch - Will cast to {splittedDeviceName[1]}", LogLevel.debug);
                
                device = GetVRDeviceByName(splittedDeviceName[0]);
                switch_type = Type.GetType($"{splittedDeviceName[1]},Assembly-CSharp");

                if (device == null)
                {
                    Log($"VRSwitch {splittedDeviceName[0]} of type {splittedDeviceName[1]} not initialized yet ....", LogLevel.debug);
                    MonoBehaviour[] devices = (MonoBehaviour[])MonoBehaviour.FindObjectsOfType(switch_type);

                    if (devices != null && devices.Length > 0)
                    {
                        // Identify the instance based on the instance ID provided (InstanceID)
                        device = Array.Find(devices, s => s.name.Equals(splittedDeviceName[0]));

                        if (device !=null)
                        {
                            Log($"VRSwitch {splittedDeviceName[0]} of type {splittedDeviceName[1]} initialized and added to the list : {device.name}", LogLevel.debug);
                            AppendVRDevice(splittedDeviceName[0], device);
                        }
                        else Log($"Warning : could not initialize VRSwitch {splittedDeviceName[0]} of type {splittedDeviceName[1]}!");
                    }
                }
                else Log($"... VRSwith {splittedDeviceName[0]} of type {splittedDeviceName[1]} already initialized!", LogLevel.debug);
            }
            else Log($"Can't find VRSwitch named {switch_name} in the supported dictonary");
        }

        public override void Update()
        {
            if ((device != null) && (Switch.value is float))
            {
                dynamic castedDevice = Convert.ChangeType(device, switch_type);

                switch (action)
                {
                    case VRSwitchAction.on:
                        if (castedDevice.currentState != castedDevice.states - 1) castedDevice.RemoteSetState(castedDevice.states - 1);
                        break;
                    case VRSwitchAction.off:
                        if (castedDevice.currentState != 0) castedDevice.RemoteSetState(0);
                        break;
                    case VRSwitchAction.toggle:
                        // Hack to get AH94 3-states switches to work properly (APU and Battery)
                        if ((castedDevice.states == 3) && (castedDevice.currentState==1))
                        {
                            _3StatesSwitch_state = 2 - _3StatesSwitch_state;
                            castedDevice.RemoteSetState(_3StatesSwitch_state);
                        }   
                        else
                            castedDevice.RemoteSetState(1 - castedDevice.currentState);
                        break;
                    case VRSwitchAction.up:
                        if (castedDevice.currentState != 0) castedDevice.RemoteSetState(castedDevice.currentState-1);
                        break;
                    case VRSwitchAction.down:
                        if (castedDevice.currentState < castedDevice.states-1) castedDevice.RemoteSetState(castedDevice.currentState + 1);
                        break;
                }
                 
            }

            base.Update();
        }

        public override void ValueChanged(IVRActor sender, object oldValue)
        {      
            if ((sender.value != oldValue) && ((float)sender.value > 0))
                if (!IsDirtyComponent(this)) 
                    AppendDirtyComponent(this);
        }
    }
}
