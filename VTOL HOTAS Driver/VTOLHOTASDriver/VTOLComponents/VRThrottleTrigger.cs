﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    class VRThrottleTrigger : VRPushButton //VRContinuousComponent
    {
        private FieldInfo miTriggerIsDown;
        public VRThrottleTrigger(String name, ILogger logger = null) : base(name, logger)
        {
            BaseVRActor actor = new BaseVRActor("ThrottleTrigger");
            actor.setParent(this);
            AddActor(actor);
        }

        public override void Initialize()
        {
            Log($"Initializing VRThrottle {name}", LogLevel.debug);

            String deviceName;
            bool componentFound = VRThrottleCore.FindDeviceNameById(name, out deviceName);

            if (componentFound)
            {
                Log($"VRThrottle - Found {deviceName} for {name}", LogLevel.debug);

                device = GetVRDeviceByName(deviceName);
                if (device == null)
                {
                    Log($"VRThrottle {deviceName} not initialized yet ....", LogLevel.debug);
                    MonoBehaviour[] devices = (MonoBehaviour[])MonoBehaviour.FindObjectsOfType<VRThrottle>();

                    if (devices != null && devices.Length > 0)
                    {
                        // Identify the instance based on the instance ID provided (InstanceID)
                        device = Array.Find(devices, s => s.name.Equals(deviceName));

                        if (device != null)
                        {
                            Log($"VRThrottle {deviceName} initialized and added to the list : {device.name}", LogLevel.debug);
                            AppendVRDevice(deviceName, device);
                        }
                        else Log($"Warning : could not initialize VRThrottle {deviceName}!");
                    }
                }
                else Log($"... VRThrottle {deviceName} already initialized!", LogLevel.debug);

                miTriggerIsDown = (FieldInfo)GetMemberInfo("triggerIsDown");
            }
            else Log($"Can't find VRThrottle named {name} in the supported dictonary");
        }

        public BaseVRActor ThrottleTrigger
        {
            get { return (BaseVRActor)GetActorByName("ThrottleTrigger"); }
        }

        public override void Update()
        {
            if (device != null)
            {
                ((VRThrottle)device).OnTriggerAxis.Invoke((float)ThrottleTrigger.value);
                if ((float)ThrottleTrigger.value >= 0.5)
                    TriggerIsDown(true);
                else
                    TriggerIsDown(false);
                    
            }

            base.Update();
        }
       
        private void TriggerIsDown(bool down)
        {
            if ((device != null) && (miTriggerIsDown != null))
                miTriggerIsDown.SetValue(device, down);
        }
    }
}
