﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    class VRStickTouchPad : VRPushButton
    {
  
        public VRStickTouchPad(String name, ILogger logger = null) : base(name, logger)
        {
            BaseVRActor actor = new BaseVRActor("StickTouchPad");
            actor.setParent(this);
            AddActor(actor);
        }

        public BaseVRActor StickTouchPad
        {
            get { return (BaseVRActor)GetActorByName("StickTouchPad"); }
        }

        public override void Initialize()
        {
            Log("Initializing VRStick...", LogLevel.debug);
            device = GetVRDeviceByName("joyInteractable");

            if (device == null)
            {
                Log("VRStick not initialized yet ....", LogLevel.debug);
                device = MonoBehaviour.FindObjectOfType<VRJoystick>();

                if (device != null)
                {
                    Log($"VRStick initialized and added to the list : {device.name}", LogLevel.debug);
                    AppendVRDevice("joyInteractable", device);
                }
                else Log("Warning : could not initialize VRStick!");

            }
            else Log("... VRStick already initialized!", LogLevel.debug);
        }

        public override void Update()
        {
            if ((device != null) && (StickTouchPad.value is Vector3))
            {
                
                if ((Vector3)StickTouchPad.value != new Vector3(0, 0, 0)) 
                {
                    ((VRJoystick)device).OnSetThumbstick.Invoke((Vector3)StickTouchPad.value);
                }
                else
                {
                    ((VRJoystick)device).OnResetThumbstick.Invoke();
                    base.Update();  // call the base update only when releasing button
                }
                    
            }

            
        }
    }
}
