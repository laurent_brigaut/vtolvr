﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    class VREjectHandle : VRPushButton
    {
        public VREjectHandle(String name, ILogger logger = null) : base(name, logger)
        {
            BaseVRActor actor = new BaseVRActor("EjectHandle");
            actor.setParent(this);
            AddActor(actor);
        }

        public BaseVRActor EjectHandle
        {
            get { return (BaseVRActor)GetActorByName("EjectHandle"); }
        }

        public override void Initialize()
        {
            Log("Initializing EjectHandle...", LogLevel.debug);
            device = GetVRDeviceByName("EjectHandle");

            if (device == null)
            {
                Log("EjectHandle not initialized yet ....", LogLevel.debug);
                device = MonoBehaviour.FindObjectOfType<EjectHandle>();

                if (device != null)
                {
                    Log($"EjectHandle initialized and added to the list : {device.name}", LogLevel.debug);
                    AppendVRDevice("EjectHandle", device);
                }
                else Log("Warning : could not initialize EjectHandle!");

            }
            else Log("... EjectHandle already initialized!", LogLevel.debug);
        }

        public override void Update()
        {
            if ((device != null) && (EjectHandle.value is float))
            {
                if ((float)EjectHandle.value > 0)
                {
                    ((EjectHandle)device).OnHandlePull.Invoke();
                }
            }

            base.Update();
        }
    }
}
