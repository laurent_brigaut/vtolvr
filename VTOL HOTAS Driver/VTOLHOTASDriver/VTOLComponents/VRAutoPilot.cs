﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    public enum VRAutoPilotAction
    {
        toggleHover,
        toggleAlt,
        toggleNav,
        toggleHeading,
        toggleSpeed,
        allOff
    }

    class VRAutoPilot : VRBaseComponent
    {
        public VRAutoPilotAction action { get; set; }

        public VRAutoPilot(String name, String apAction, ILogger logger = null) : base(name + apAction, logger)
        {
            VRAutoPilotAction a;
            if (Enum.TryParse<VRAutoPilotAction>(apAction, true, out a))
                action = a;
            else
            {
                action = VRAutoPilotAction.allOff;
                this.name = name + action.ToString();
            }

            BaseVRActor actor = new BaseVRActor("AutoPilot");
            actor.setParent(this);
            AddActor(actor);
        }

        public BaseVRActor AutoPilot
        {
            get { return (BaseVRActor)GetActorByName("AutoPilot"); }
        }

        public override void Initialize()
        {
            Log("Initializing VTOLAutoPilot...", LogLevel.debug);
            device = GetVRDeviceByName("autoPilot");

            if (device == null)
            {
                Log("VTOLAutoPilot not initialized yet ....", LogLevel.debug);
                device = MonoBehaviour.FindObjectOfType<VTOLAutoPilot>();

                if (device != null)
                {
                    Log($"VTOLAutoPilot initialized and added to the list : {device.name}", LogLevel.debug);
                    AppendVRDevice("autoPilot", device);
                }
                else Log("Warning : could not initialize VTOLAutoPilot!");

            }
            else Log("... VTOLAutoPilot already initialized!", LogLevel.debug);
        }

        public override void Update()
        {
            if ((device != null) && (AutoPilot.value is float))
            {
                switch (action)
                {
                    case VRAutoPilotAction.allOff:
                        ((VTOLAutoPilot)device).AllOff();
                        break;
                    case VRAutoPilotAction.toggleAlt:
                        ((VTOLAutoPilot)device).ToggleAltitudeHold();
                        break;
                    case VRAutoPilotAction.toggleHeading:
                        ((VTOLAutoPilot)device).ToggleHeadingHold();
                        break;
                    case VRAutoPilotAction.toggleHover:
                        ((VTOLAutoPilot)device).ToggleHoverMode();
                        break;
                    case VRAutoPilotAction.toggleNav:
                        ((VTOLAutoPilot)device).ToggleNav();
                        break;
                    case VRAutoPilotAction.toggleSpeed:
                        ((VTOLAutoPilot)device).ToggleSpeedHold();
                        break;
                }
            }

            base.Update();
        }

        public override void ValueChanged(IVRActor sender, object oldValue)
        {
            if ((sender.value != oldValue) && ((float)sender.value > 0))
                if (!IsDirtyComponent(this))
                    AppendDirtyComponent(this);
        }
    }
}
