﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    class VRThrottleMenu : VRPushButton
    {

        public VRThrottleMenu(String name, ILogger logger = null) : base(name, logger)
        {
            BaseVRActor actor = new BaseVRActor("ThrottleMenu");
            actor.setParent(this);
            AddActor(actor);
        }

        public BaseVRActor ThrottleMenu
        {
            get { return (BaseVRActor)GetActorByName("ThrottleMenu"); }
        }

        public override void Initialize()
        {
            Log($"Initializing VRThrottle {name}", LogLevel.debug);

            String deviceName;
            bool componentFound = VRThrottleCore.FindDeviceNameById(name, out deviceName);

            if (componentFound)
            {
                Log($"VRThrottle - Found {deviceName} for {name}", LogLevel.debug);

                device = GetVRDeviceByName(deviceName);
                if (device == null)
                {
                    Log($"VRThrottle {deviceName} not initialized yet ....", LogLevel.debug);
                    MonoBehaviour[] devices = (MonoBehaviour[])MonoBehaviour.FindObjectsOfType<VRThrottle>();

                    if (devices != null && devices.Length > 0)
                    {
                        // Identify the instance based on the instance ID provided (InstanceID)
                        device = Array.Find(devices, s => s.name.Equals(deviceName));

                        if (device != null)
                        {
                            Log($"VRThrottle {deviceName} initialized and added to the list : {device.name}", LogLevel.debug);
                            AppendVRDevice(deviceName, device);
                        }
                        else Log($"Warning : could not initialize VRThrottle {deviceName}!");
                    }
                }
                else Log($"... VRThrottle {deviceName} already initialized!", LogLevel.debug);
            }
            else Log($"Can't find VRThrottle named {name} in the supported dictonary");
        }


        public override void Update()
        {
            if ((device != null) && (ThrottleMenu.value is float))
            {
                if ((float)ThrottleMenu.value > 0)
                    ((VRThrottle)device).OnMenuButtonDown.Invoke();
                else
                    ((VRThrottle)device).OnMenuButtonUp.Invoke();
            }

            base.Update();
        }
    }
}
