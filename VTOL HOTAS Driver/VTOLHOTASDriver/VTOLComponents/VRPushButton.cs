﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTOLHOTASDriver
{
    // Component needs updating on state changed (pressed/depressed)
    class VRPushButton : VRBaseComponent
    {
        public VRPushButton(String name, ILogger logger = null) : base(name, logger)
        {
        }

        public override void ValueChanged(IVRActor sender, object oldValue)
        {           
            // Adds the component only if state changed
            if (sender.value != oldValue)
            {
                if (!IsDirtyComponent(this))
                    AppendDirtyComponent(this);
                
            }
        }
    }
}
