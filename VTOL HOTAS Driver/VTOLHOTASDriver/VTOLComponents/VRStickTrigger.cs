﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    class VRStickTrigger : VRPushButton
    {

        public VRStickTrigger(String name, ILogger logger = null) : base(name, logger)
        {
            BaseVRActor actor = new BaseVRActor("StickTrigger");
            actor.setParent(this);
            AddActor(actor);
        }

        public BaseVRActor StickTrigger
        {
            get { return (BaseVRActor)GetActorByName("StickTrigger"); }
        }

        public override void Initialize()
        {
            Log("Initializing VRStick...", LogLevel.debug);
            device = GetVRDeviceByName("joyInteractable");

            if (device == null)
            {
                Log("VRStick not initialized yet ....", LogLevel.debug);
                device = MonoBehaviour.FindObjectOfType<VRJoystick>();

                if (device != null)
                {
                    Log($"VRStick initialized and added to the list : {device.name}", LogLevel.debug);
                    AppendVRDevice("joyInteractable", device);
                }
                else Log("Warning : could not initialize VRStick!");

            }
            else Log("... VRStick already initialized!", LogLevel.debug);
        }


        public override void Update()
        {
            if ((device != null) && (StickTrigger.value is float))
            {
                if ((float)StickTrigger.value > 0)
                {
                    ((VRJoystick)device).OnTriggerAxis.Invoke((float)StickTrigger.value); // support for F45 trigger
                    ((VRJoystick)device).OnTriggerDown.Invoke();
                }
                    
                else
                    ((VRJoystick)device).OnTriggerUp.Invoke();
            }

            base.Update();
        }
    }
}
