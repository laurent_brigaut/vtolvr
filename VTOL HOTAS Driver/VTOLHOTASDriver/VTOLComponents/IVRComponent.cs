﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VTOLHOTASDriver
{
    public interface IVRComponent
    {
        string name { get; }
        MonoBehaviour device { get;  }
      
        void AddActor(IVRActor actor);
        IVRActor GetActorByName(string name);

        void Update();
        void ValueChanged(IVRActor sender, object oldValue);
        void Initialize();
    }
}