﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using SharpDX.DirectInput;

namespace vtol_hotas_tool
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static DirectInput DiInstance = new DirectInput();
        public List<GameController> controllers = new List<GameController>();
        public List<GameControllerActor> actorsForDatagrid = new List<GameControllerActor>();
       
        private DispatcherTimer _timer = new DispatcherTimer();

        public MainWindow()
        {
            InitializeComponent();

            LoadControllers();

            controllersListWidget.ItemsSource = controllers;

            // Start tmer proc used to poll currently selected device input
            _timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(100) };
            _timer.Tick += _timerProc;
            _timer.Start();
        }

        // Load gaming devices plugged to the computer
        private void LoadControllers()
        {
            var diDeviceInstances = DiInstance.GetDevices();

            // Clears the actual list to make sure it's empty
            controllers.Clear();

            foreach (var controller in diDeviceInstances)
            {
                if (!IsStickType(controller)) continue;

                var foundStick = new Joystick(DiInstance, controller.InstanceGuid);
                controllers.Add(new GameController { controller = foundStick }); // assuming that devices have different names here!
            }
        }

        private void LoadActors(GameController controller)
        {
            List<DeviceObjectInstance> actors = (List<DeviceObjectInstance>)controller.controller.GetObjects();
            foreach (var actor in actors)
            {
                actorsForDatagrid.Add(new GameControllerActor { actor = actor });

            }
        }


        private void resetButton_Click(object sender, RoutedEventArgs e)
        {
            controllers.Clear();
            LoadControllers();

            controllersListWidget.ItemsSource = null;
            controllersListWidget.ItemsSource = controllers;
        }


        private void devicesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GameController controller;

            // a controller was selected before the selection change vent
            if (e.RemovedItems.Count != 0)
            {
                controller = (GameController)e.RemovedItems[0];
                controller.controller.Unacquire();
                selectedDeviceTextBlockWidget.Text = "";
                controllerActorsListWidget.ItemsSource = null;
                actorsForDatagrid.Clear();
            }

            // a new controller is selected
            if (e.AddedItems.Count != 0)
            {
                controller = (GameController)controllersListWidget.SelectedItem;
                selectedDeviceTextBlockWidget.Text = controller.name;
                controller.controller.Properties.BufferSize = 512;
                controller.controller.Acquire();

                LoadActors(controller);

                controllerActorsListWidget.ItemsSource = actorsForDatagrid;


            }
        }

        
        private bool IsStickType(DeviceInstance deviceInstance)
        {
            return deviceInstance.Type == SharpDX.DirectInput.DeviceType.Joystick
                   || deviceInstance.Type == SharpDX.DirectInput.DeviceType.Gamepad
                   || deviceInstance.Type == SharpDX.DirectInput.DeviceType.FirstPerson
                   || deviceInstance.Type == SharpDX.DirectInput.DeviceType.Flight
                   || deviceInstance.Type == SharpDX.DirectInput.DeviceType.Driving
                   || deviceInstance.Type == SharpDX.DirectInput.DeviceType.Supplemental;
        }

        void _timerProc(object sender, EventArgs e)
        {
           
            GameController controller = (GameController)controllersListWidget.SelectedItem;
            if (controller != null)
            {
                var data = controller.controller.GetBufferedData();
        
                foreach (var state in data)
                {
                    // look for the control basd on the offset
                    var ctrlInfo = controller.controller.GetObjectInfoByName(state.Offset.ToString());
                    if (ctrlInfo != null)
                    {
                        GameControllerActor actor = actorsForDatagrid.Find(x => x.offset == ctrlInfo.Offset);
                        if (actor != null)
                        {
                            actor.value = state.Value;
                            actor.sharpDXName = state.Offset.ToString();

                            
                        }
                    }
                }
            }
        }

    }

    public class GameController
    {
        public Joystick controller { get; set; }

        public String name
        {
            get
            {
                return controller.Information.ProductName;
            }
        }
    }

    public class GameControllerActor : INotifyPropertyChanged

    {
        public DeviceObjectInstance actor { get; set; }

        public String name
        {
            get
            {
                return actor.Name;
            }
        }


        public int offset 
        { 
            get
            {
                return actor.Offset;
            }
        }


        private int Value;
        public int value
        {
            get
            {
                return Value;
            }

            set
            {
                Value = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null) handler(this, new PropertyChangedEventArgs("value"));
            }
        }

        private String _sharpDXName;
        public String sharpDXName
        {
            get
            {
                return _sharpDXName;
            }

            set
            {
                _sharpDXName = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("sharpDXName"));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

}
