﻿
namespace DebugApp
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.CommadtextBox = new System.Windows.Forms.TextBox();
            this.ConsoletextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // CommadtextBox
            // 
            this.CommadtextBox.Location = new System.Drawing.Point(12, 12);
            this.CommadtextBox.Name = "CommadtextBox";
            this.CommadtextBox.Size = new System.Drawing.Size(776, 20);
            this.CommadtextBox.TabIndex = 0;
            this.CommadtextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CommadtextBox_KeyUp);
            // 
            // ConsoletextBox
            // 
            this.ConsoletextBox.Location = new System.Drawing.Point(12, 38);
            this.ConsoletextBox.Multiline = true;
            this.ConsoletextBox.Name = "ConsoletextBox";
            this.ConsoletextBox.ReadOnly = true;
            this.ConsoletextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ConsoletextBox.Size = new System.Drawing.Size(776, 384);
            this.ConsoletextBox.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ConsoletextBox);
            this.Controls.Add(this.CommadtextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CommadtextBox;
        private System.Windows.Forms.TextBox ConsoletextBox;
    }
}

