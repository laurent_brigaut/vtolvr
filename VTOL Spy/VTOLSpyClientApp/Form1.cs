﻿using DebuggingTools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebugApp
{
    public partial class Form1 : Form
    {
        private DebugClient _debugClient;
        private List<String> _cmdHistory = new List<string>();
        private int _cmdHistoryCurrentIndex = 0;
 
        delegate void SetTextCallback(string text);

        public void DebugMessageReceived(string message)
        {
            if (ConsoletextBox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(AppendTextConsole);
                this.Invoke(d, new object[] { $"{message}" });
            }
            else
            {
                AppendTextConsole($"{message}");
            }

        }

        public Form1()
        {
            InitializeComponent();

            _debugClient = new DebugClient("127.0.0.1", 11000);
            _debugClient.MessageReceivedCallBack = DebugMessageReceived;

        }

        private void AppendTextConsole(String t)
        {
            ConsoletextBox.AppendText($"{t}{Environment.NewLine}");
        }

        private void CommadtextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                String command = ((TextBox)sender).Text;

                _cmdHistory.Add(command);
                _cmdHistoryCurrentIndex = _cmdHistory.Count;

                ((TextBox)sender).Text = "";

                AppendTextConsole($"> {command}");

                if (command.ToLower() == "connect")
                {
                    AppendTextConsole("Connecting to Socket Server ...");
                    _debugClient.Connect();
                }
                else if (command.ToLower() == "disconnect")
                {
                    AppendTextConsole("Disconnecting from Socket Server ...");
                    _debugClient.Close();
                }
                else if (command.ToLower() == "cls")
                {
                    ConsoletextBox.Text = "";
                }
                else
                {

                    _debugClient.SendMessage(command);
                }

                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Up)
            {
                if ((_cmdHistoryCurrentIndex >= 1) && (_cmdHistoryCurrentIndex <= _cmdHistory.Count))
                {
                    _cmdHistoryCurrentIndex--;
                    
                    ((TextBox)sender).Text = _cmdHistory[_cmdHistoryCurrentIndex];
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                    ((TextBox)sender).SelectionLength = 0;
                }
                else
                {
                    _cmdHistoryCurrentIndex = _cmdHistory.Count;
                    ((TextBox)sender).Text = "";
                }

                
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Down)
            {
                if ((_cmdHistoryCurrentIndex >= 1) && (_cmdHistoryCurrentIndex < _cmdHistory.Count - 1))
                {
                    _cmdHistoryCurrentIndex++;
                    ((TextBox)sender).Text = _cmdHistory[_cmdHistoryCurrentIndex];
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                    ((TextBox)sender).SelectionLength = 0;
                }
                else
                {
                    _cmdHistoryCurrentIndex = _cmdHistory.Count;
                    ((TextBox)sender).Text = "";
                }
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Escape)
            {
                _cmdHistoryCurrentIndex = _cmdHistory.Count;
                ((TextBox)sender).Text = "";
            }
        }
    }
}
