﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DebuggingTools
{
    class GetMemberInfoCmd : ReflectionCommand
    {

        protected String memberName;
        public GetMemberInfoCmd(String objClass, String name): base(objClass)
        { 
            memberName = name; 
        }

        public override IEnumerator Run()
        {
            MemberInfo[] memberInfo = null;
            yield return GetMember(memberName, out memberInfo);
            
            if (memberInfo != null)
            {
                foreach (var member in memberInfo)
                {
                    yield return SendAsyncMessage($"{member.Name}");
                    yield return SendAsyncMessage($"   - Declared in : {member.DeclaringType.ToString()}");
                    yield return SendAsyncMessage($"   - Type : {member.MemberType.ToString()}");

                    switch (member.MemberType)
                    {
                        case MemberTypes.Method:
                            yield return SendAsyncMessage($"   - Static : {((MethodInfo)member).IsStatic.ToString()}");
                            yield return SendAsyncMessage($"   - Return : Type={((MethodInfo)member).ReturnParameter.ParameterType}");

                            foreach (ParameterInfo pi in ((MethodInfo)member).GetParameters())
                            {
                                yield return SendAsyncMessage($"   - Parameter : Type={pi.ParameterType}, Name={pi.Name}");
                            }
                            break;

                        case MemberTypes.Property:
                            foreach (MethodInfo am in ((PropertyInfo)member).GetAccessors())
                            {
                                yield return SendAsyncMessage($"   - Accessor method : {am}");
                            }
                            break;

                        case MemberTypes.Field:
                            yield return SendAsyncMessage($"   - Field Type : {((FieldInfo)member).FieldType.ToString()}");
                            yield return SendAsyncMessage($"   - Static : {((FieldInfo)member).IsStatic.ToString()}");
                            break;
                    }
                }
            }
        }
    }
}
