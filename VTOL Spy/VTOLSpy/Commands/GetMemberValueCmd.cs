﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DebuggingTools
{
    class GetMemberValueCmd : ReflectionCommand
    {

        protected int InstanceID;
        protected String MemberInfoName;

        public GetMemberValueCmd(String objClass, String instance, String member) : base(objClass)
        {
            if (!int.TryParse(instance, out InstanceID))
                InstanceID = -1;
            MemberInfoName = member;
        }

        public override IEnumerator Run()
        {
            UnityEngine.Object[] objects = null;
            yield return FindObjectsOfType(out objects);

            if (objects != null && objects.Length > 0)
            {
                UnityEngine.Object instance = Array.Find(objects, s => s.GetInstanceID().Equals(InstanceID));
                yield return null;

                if (instance == null)
                    yield return SendAsyncMessage($"Class {objectClass} : no instance found for {InstanceID.ToString()}");
                else
                {
                    MemberInfo[] memberInfo = null;
                    yield return GetMember(MemberInfoName, out memberInfo);

                    if (memberInfo != null)
                    {
                        switch (memberInfo[0].MemberType)
                        {
                            case MemberTypes.Property:
                                yield return SendAsyncMessage($"Value : {((PropertyInfo)memberInfo[0]).GetValue(instance)}");
                                break;

                            case MemberTypes.Field:
                                yield return SendAsyncMessage($"Value : {((FieldInfo)memberInfo[0]).GetValue(instance)}");
                                break;

                            default:
                                yield return SendAsyncMessage($"Class {objectClass} : member {MemberInfoName} not found for {InstanceID.ToString()}");
                                break;
                        }
                        
                    }
                }
                            
            }
            else yield return SendAsyncMessage($"Class {objectClass} Not Found!");
        }
    }
}
