﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DebuggingTools
{
    class SetMemberValueCmd : ReflectionCommand
    {
        protected int InstanceID;
        protected String MemberInfoName;
        protected String MemberValue;

        public SetMemberValueCmd(String objClass, String instance, String member, String value):base(objClass)
        {
            if (!int.TryParse(instance, out InstanceID))
                InstanceID = -1;
            MemberInfoName = member;
            MemberValue = value;
        }

        public override IEnumerator Run()
        {
            UnityEngine.Object[] objects = null;
            yield return FindObjectsOfType(out objects);

            if (objects != null && objects.Length > 0)
            {
                UnityEngine.Object instance = Array.Find(objects, s => s.GetInstanceID().Equals(InstanceID));
                yield return null;

                if (instance == null)
                    yield return SendAsyncMessage($"Class {objectClass} : no instance found for {InstanceID}");
                else
                {
                    MemberInfo[] memberInfo = null;
                    yield return GetMember(MemberInfoName, out memberInfo);

                    if (memberInfo != null)
                    {
                        switch (memberInfo[0].MemberType)
                        {
                            case MemberTypes.Property:
                                ((PropertyInfo)memberInfo[0]).SetValue(instance, Convert.ChangeType(MemberValue, ((PropertyInfo)memberInfo[0]).PropertyType), null);

                                break;

                            case MemberTypes.Field:
                                ((FieldInfo)memberInfo[0]).SetValue(instance, Convert.ChangeType(MemberValue, ((FieldInfo)memberInfo[0]).FieldType));

                                break;

                            default:
                                DebugCommand.DebugServer.SendMessage($"Class {objectClass} : member {MemberInfoName} not found for {InstanceID}");
                                break;
                        }
                    }
                }
            }
            else yield return SendAsyncMessage($"Class {objectClass} Not Found!");

        }
    }
}
