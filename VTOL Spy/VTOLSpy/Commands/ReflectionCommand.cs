﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DebuggingTools
{
    class ReflectionCommand : DebugCommand
    {
        protected String objectClass {
            get
            {
                if (_objClassType != null)
                    return _objClassType.Name;
                else
                    return null;
            }

            set
            {
                _objClassType = Type.GetType($"{value},Assembly-CSharp");
            }
        }

        private Type _objClassType = null;

        public ReflectionCommand(String objClass)
        {
            objectClass = objClass;
        }

        public WaitForSeconds GetMember(String name, out MemberInfo[] memberInfo)
        {
            memberInfo = null;

            if (_objClassType != null)
            {
                memberInfo = _objClassType.GetMember(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);

                if (memberInfo.Length == 0)
                {
                    memberInfo = null;
                    DebugCommand.DebugServer.SendMessage($"Warning - call to GetMember for {objectClass} returned empty result!");
                }
            }
            else DebugCommand.DebugServer.SendMessage("Error - Try to call GetMember with null or invalid class type!");

            return new WaitForSeconds((float)0.1); ;
        }

        public WaitForSeconds GetMembers(out MemberInfo[] members)
        {
            members = null;

            if (_objClassType != null)
            {
                members = _objClassType.GetMembers(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);

                if (members.Length == 0)
                {
                    members = null;
                    DebugCommand.DebugServer.SendMessage($"Warning - call to GetMembers for {objectClass} returned empty result!");
                }
            }
            else DebugCommand.DebugServer.SendMessage("Error - Try to call GetMembers with null or invalid class type!");

            return new WaitForSeconds((float)0.1); ;
        }

        public WaitForSeconds FindObjectsOfType(out UnityEngine.Object[] objects)
        {
            return FindObjectsOfType(_objClassType, out objects);
        }

        public WaitForSeconds FindObjectsOfType(String objClass, out UnityEngine.Object[] objects)
        {
            return FindObjectsOfType(Type.GetType($"{objClass},Assembly-CSharp"), out objects);
        }

        public WaitForSeconds FindObjectsOfType(Type objClassType, out UnityEngine.Object[] objects)
        {
            objects = null;

            if (objClassType != null)
            {
                Type returnType = typeof(UnityEngine.Object);
                MethodInfo method = returnType?.GetMethod(nameof(UnityEngine.Object.FindObjectsOfType), new Type[1] { typeof(Type) });
                System.Object result = method?.Invoke(null, new System.Object[1] { objClassType });

                objects = (UnityEngine.Object[])result;
            }

            return new WaitForSeconds((float)0.1); ;
        }

        public WaitForSeconds FindObjectByInstanceID(String objClass, String instanceID, out UnityEngine.Object obj)
        {
            int id;

            if (!int.TryParse(instanceID, out id))
                id = -1;

            FindObjectByInstanceID(objClass, id, out obj);

            return new WaitForSeconds((float)0.1); ;
        }

        public WaitForSeconds FindObjectByInstanceID(String objClass, int instanceID, out UnityEngine.Object obj)
        {
            obj = null;

            UnityEngine.Object[] objects = null;
            FindObjectsOfType(objClass, out objects);

            // Check if instances of Class (objClass) exist? 
            if (objects != null && objects.Length > 0)
            {
                // Identify the instance based on the instance ID provided (InstanceID)
                obj = Array.Find(objects, s => s.GetInstanceID().Equals(instanceID));
            }

            return new WaitForSeconds((float)0.1); ;
        }

        public WaitForSeconds SendAsyncMessage(string message)
        {
            DebugCommand.DebugServer.SendMessage(message);
            return new WaitForSeconds((float)0.1);
        }
    }
}
