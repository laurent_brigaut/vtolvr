﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DebuggingTools
{
    class CallMemberMethodCmd : ReflectionCommand
    {
        protected int InstanceID;
        protected String MethodName;
        protected List<String> Args;

        public CallMemberMethodCmd(String objClass, String instance, String method, params String[] args) : base(objClass)
        {
            if (!int.TryParse(instance, out InstanceID))
                InstanceID = -1;
            MethodName = method;
            Args = args.ToList();
        }

        public override IEnumerator Run()  
        {
            UnityEngine.Object[] objects = null;
            yield return FindObjectsOfType(out objects);

            // Check if instances of Class (objClass) exist? 
            if (objects != null && objects.Length > 0)
            {
                // Identify the instance based on the instance ID provided (InstanceID)
                UnityEngine.Object instance = Array.Find(objects, s => s.GetInstanceID().Equals(InstanceID));
                yield return null;

                if (instance == null) // wrong instanceID
                    yield return SendAsyncMessage($"Class {objectClass} : no instance found for {InstanceID}");
                else
                {
                    // look for Method info
                    MemberInfo[] memberInfo = null;
                    yield return GetMember(MethodName, out memberInfo);

                    if (memberInfo != null) // check if Method name exists
                    {
                        // Note we support only the first member if more than one
                        if ((memberInfo[0].MemberType == MemberTypes.Method) && (!((MethodInfo)memberInfo[0]).IsStatic)) // check if member is an instance method
                        {
                            // check if number of args is consistent
                            ParameterInfo[] argsInfo= ((MethodInfo)memberInfo[0]).GetParameters();
                            if (argsInfo.Length == Args.Count())
                            {
                                yield return null;

                                List<object> argObjs = new List<object>();

                                foreach (ParameterInfo arginfo in argsInfo)
                                {
                                    yield return null;

                                    String argStr = Args.First<String>();
                                    Args.RemoveAt(0);

                                    if (argStr.StartsWith("\\(")) // passed an object instance ID ("\(object type)instanceID")
                                    {
                                        List<String> split = argStr.Substring(2).Split(')').ToList();
                                        UnityEngine.Object pi = null;
                                        FindObjectByInstanceID(split[0], split[1], out pi);
                                        argObjs.Add(pi);

                                    }
                                    else argObjs.Add(Convert.ChangeType(argStr, arginfo.ParameterType)); // basic type
                                }

                                yield return null;

                                object rt = ((MethodInfo)memberInfo[0]).Invoke(instance, argObjs.ToArray());
                                yield return SendAsyncMessage($"Return: {rt}");
                            }
                            else yield return SendAsyncMessage($"{MethodName} expects {argsInfo.Length} arguments, {Args.Count()} got passed!");
                        }
                        else yield return SendAsyncMessage($"{MethodName} of Class {objectClass} is not an instance method (either static or not a method)!");
                    }
                    else yield return SendAsyncMessage($"Method {MethodName} of Class {objectClass} Not Found!");
                }
            }
            else yield return SendAsyncMessage($"Class {objectClass} Not Found!");

        }
    }
}
