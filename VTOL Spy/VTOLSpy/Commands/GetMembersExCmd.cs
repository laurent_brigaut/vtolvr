﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DebuggingTools
{
    class GetMembersExCmd : ReflectionCommand
    {
        public GetMembersExCmd(String objClass):base(objClass)
        {
        }

        public override IEnumerator Run()
        {
          
            MemberInfo[] members = null;
            yield return GetMembers(out members);

            if (members != null)
            {
                StringBuilder textLine = new StringBuilder("", 1024);
                yield return SendAsyncMessage("Declared Class;Type;Name;Static;Public;Return Type;Other");

                foreach (var member in members)
                {
                    textLine.Append($"{member.DeclaringType.ToString()};{member.MemberType.ToString()};{member.Name}");
                    yield return null;

                    switch (member.MemberType)
                    {
                        case MemberTypes.Method:
                            textLine.Append($";{((MethodInfo)member).IsStatic.ToString()};{((MethodInfo)member).IsPublic.ToString()};{((MethodInfo)member).ReturnParameter.ParameterType};");
                            
                            foreach (ParameterInfo pi in ((MethodInfo)member).GetParameters())
                            {
                                textLine.Append($"{pi.ParameterType} {pi.Name}, ");
                                yield return null;
                            }

                            break;

                        case MemberTypes.Property:
                            if (((PropertyInfo)member).GetGetMethod() != null) 
                            {
                                textLine.Append($";{((PropertyInfo)member).GetGetMethod().IsStatic.ToString()};get:{((PropertyInfo)member).GetGetMethod().IsPublic.ToString()}");
                            }

                            if (((PropertyInfo)member).GetSetMethod() != null)
                            {
                                textLine.Append($"/set:{((PropertyInfo)member).GetSetMethod().IsPublic.ToString()}");
                            }

                            textLine.Append($";{((PropertyInfo)member).PropertyType};");

                            foreach (MethodInfo am in ((PropertyInfo)member).GetAccessors())
                            {
                                textLine.Append($"{am}, ");
                                yield return null;
                            }
                            break;

                        case MemberTypes.Field:
                            textLine.Append($";{((FieldInfo)member).FieldType.ToString()};{((FieldInfo)member).IsPublic.ToString()};{((FieldInfo)member).FieldType.ToString()}");
                            yield return null;
                            break;
                    }


                    yield return SendAsyncMessage(textLine.ToString());
                    textLine.Clear();
                }
            }
            
        }
    }
}
