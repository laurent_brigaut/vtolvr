﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DebuggingTools
{
    class GetMembersCmd : ReflectionCommand
    {
        public GetMembersCmd(String objClass):base(objClass)
        {
        }

        public override IEnumerator Run()
        {
          
            MemberInfo[] members = null;
            yield return GetMembers(out members);

            if (members != null)
            {
                yield return SendAsyncMessage($"Class {objectClass} - Found {members.Length} public members!");

                foreach (var member in members)
                {
                     yield return SendAsyncMessage($"{member.MemberType.ToString()} - {member.Name} ({member.DeclaringType.ToString()})");
                }
            }
            
        }
    }
}
