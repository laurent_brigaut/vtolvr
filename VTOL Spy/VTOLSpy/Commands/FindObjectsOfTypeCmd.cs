﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DebuggingTools
{
    class FindObjectsOfTypeCmd : ReflectionCommand
    {
        public FindObjectsOfTypeCmd(String objClass):base(objClass) 
        {
        }

        public override IEnumerator Run()
        {
            UnityEngine.Object[] objects = null;
            yield return FindObjectsOfType(out objects);

            if (objects.Length == 0)
                yield return SendAsyncMessage($"Class {objectClass} : 0 instance found!");
            else
            {
                yield return SendAsyncMessage($"Class {objectClass} : {objects.Length} instances(s) Found");
                foreach (var obj in objects)
                {
                    yield return SendAsyncMessage($"Name: {obj.name} InstanceID: {obj.GetInstanceID()}");
                }
            }
        }
    }
}
