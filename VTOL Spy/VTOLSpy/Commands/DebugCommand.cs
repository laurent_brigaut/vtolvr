﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VTOLSpy;

namespace DebuggingTools
{
    internal delegate IEnumerator RunAction();

    class DebugCommand
    {
        private static object _lockObject = new object();
        private static readonly Queue<RunAction> _commands = new Queue<RunAction>();

        public static DebugServer DebugServer { get; internal set; }
        public static ILogger Logger{ get; internal set; }

        public static void InvokeAsync(RunAction action)
        {
            lock (_lockObject)
            {
                _commands.Enqueue(action);
            }
        }

        public static RunAction Update()
        {
            lock (_lockObject)
            {
                if (_commands.Count > 0)
                {
                    return _commands.Dequeue();
                }
            }

            return null;
        }

        public static DebugCommand ParseCommand(string message)
        {
            // Split command and arguments (separated by space)
            List<String> split = message.Split(' ').ToList();

            String cmd = split[0];
            split.RemoveAt(0);

            foreach (var s in split) Log($"arg : {s}");

            Object[] args = split.ToArray<Object>();

            DebugCommand dbg_cmd = null;
            Type dbg_cmd_type = Type.GetType($"DebuggingTools.{cmd}Cmd");

            if ((dbg_cmd_type == null) && (DebugServer != null))
                DebugServer.SendMessage($"{cmd} : Unknown Command!");
            else
            {
                dbg_cmd = (DebugCommand)Activator.CreateInstance(dbg_cmd_type, args);
            }
                
            return dbg_cmd;
        }

        public static void Log(object message)
        {
            if (Logger != null)
            {
                Logger._Log(message);
            }
        }

        virtual public IEnumerator Run()
        {
            throw new NotImplementedException();
        }
    }
}
