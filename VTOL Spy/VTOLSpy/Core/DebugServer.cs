﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DebuggingTools
{
    class DebugServer : SocketTools
    {
        protected Socket client;

        public DebugServer(string ip, int port)
        {
            IPAddress ipAddress = IPAddress.Parse(ip);
            endPoint = new IPEndPoint(ipAddress, port);

            // Create a TCP/IP socket.  
            socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint with one client connection max
            socket.Bind(endPoint);
            socket.Listen(1);

            // start listening for connection
            socket.BeginAccept(new AsyncCallback(AcceptCallback), socket);

        }

        private void AcceptCallback(IAsyncResult ar)
        {
            // Get the socket that handles the client request.  
            Socket listener = (Socket)ar.AsyncState;
            client = listener.EndAccept(ar);

            // start reading incoming messages
            BeginRead();

            // start listening for future connections
            socket.BeginAccept(new AsyncCallback(AcceptCallback), socket);
        }

        public override void SendMessage(string message)
        {
            SendMessage(client, message);
        }

        public override void BeginRead()
        {
            BeginRead(client);
        }

    }
}
