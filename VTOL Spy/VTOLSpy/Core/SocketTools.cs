﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DebuggingTools
{
    internal delegate void MessageReceivedCallBack(string message);
    internal delegate void MessageSentCallBack(bool success);

    internal class StateObject
    {
        // Client socket.  
        public Socket workSocket = null;
        // Size of receive buffer.  
        public const int BufferSize = 256;
        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];
        // Received data string.  
        public StringBuilder sb = new StringBuilder();
    }

    class SocketTools
    {
        protected Socket socket;
        protected IPEndPoint endPoint;

        public MessageReceivedCallBack MessageReceivedCallBack { get; set; }
        public MessageSentCallBack MessageSentCallBack { get; set; }


        public virtual void SendMessage(String message)
        {
            SendMessage(socket, message);
        }

        public virtual void SendMessage(Socket sck, String message)
        {
            if ((sck != null) && (sck.Connected))
            {
                byte[] byteData = Encoding.ASCII.GetBytes($"{message}</EOM>");

                // Begin sending the data to the remote device.  
                sck.BeginSend(byteData, 0, byteData.Length, 0,
                    new AsyncCallback(SendCallback), sck);
            }
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = handler.EndSend(ar);

                MessageSentCallBack?.Invoke(true);

            }
            catch
            {
            }
            
        }

        public virtual void BeginRead()
        {
            BeginRead(socket);
        }

        public virtual void BeginRead(Socket sck)
        {
            if ((sck != null) && (sck.Connected))
            {
                // Create the state object.  
                StateObject state = new StateObject();
                state.workSocket = sck;

                // Begin receiving the data from the remote device.  
                sck.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
            }
        }

        private void ReadCallback(IAsyncResult ar)
        {
            
            String content = String.Empty;

            // Retrieve the state object and the client socket
            // from the asynchronous state object.  
            StateObject state = (StateObject)ar.AsyncState;
            Socket socket = state.workSocket;

            // Read data from the remote device.  
            int bytesRead = socket.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There  might be more data, so store the data received so far.  
                state.sb.Append(Encoding.ASCII.GetString(
                    state.buffer, 0, bytesRead));

                // Check for end-of-message tag. If it is not there, read
                // more data.  
                content = state.sb.ToString();
                if (content.IndexOf("</EOM>") > -1)
                {
                    MessageReceivedCallBack?.Invoke(content.Substring(0, content.IndexOf("</EOM>")));

                    // remove message from the string builder
                    state.sb.Clear();
                    state.sb.Append(content.Substring(content.IndexOf("</EOM>")+6));
                }
                  
            }

            // continue reading
            socket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);

        }

        public void Close()
        {
            if (socket != null)
            {
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }
        }
    }
}
