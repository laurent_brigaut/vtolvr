﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DebuggingTools
{
    public delegate void ConnectionCallBack(bool success);

    class DebugClient : SocketTools
    {
        public ConnectionCallBack ConnectionCallBack { get; set; }

        public DebugClient(string ip, int port)
        {
            IPAddress ipAddress = IPAddress.Parse(ip);
            endPoint = new IPEndPoint(ipAddress, port);

            // Create a TCP/IP socket.  
            socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Connect()
        {
            if (socket != null)
                socket.BeginConnect(endPoint, new AsyncCallback(ConnectCallback), socket);
        }

        private void ConnectCallback(IAsyncResult ar)
        {
           
            // Retrieve the socket from the state object.  
            Socket client = (Socket)ar.AsyncState;

            // Complete the connection.  
            client.EndConnect(ar);

            if (ConnectionCallBack != null)
                ConnectionCallBack(true);

            // Begin reading data ....
            BeginRead();
                      
        }
    }
}
