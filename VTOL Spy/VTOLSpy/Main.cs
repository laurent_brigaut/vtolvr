using DebuggingTools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/*
 * 
 * TODO :
 * 
 *       - Clean client disconnection (generates exceptions on client and server)
 *       - implement server activation via settings.xml
 *       - implement commands :
 *          - list scene objects of a specific type
 *          - ....
 * 
 * */

namespace VTOLSpy
{
    public class Main : VTOLMOD, ILogger
    {
        private DebugServer _debugServer;

        // This method is run once, when the Mod Loader is done initialising this game object
        public override void ModLoaded()
        {
            //This is an event the VTOLAPI calls when the game is done loading a scene
            VTOLAPI.SceneLoaded += SceneLoaded;
            base.ModLoaded();

            _debugServer = new DebugServer("127.0.0.1", 11000);
            _debugServer.MessageReceivedCallBack = DebugMessageReceived;
            DebugCommand.DebugServer = _debugServer;
            DebugCommand.Logger = this;
        }

        //This method is called every frame by Unity. Here you'll probably put most of your code
        void Update()
        {
            RunAction action = DebugCommand.Update();
            if (action != null)
                StartCoroutine(action());
        }

        //This method is like update but it's framerate independent. This means it gets called at a set time interval instead of every frame. This is useful for physics calculations
        void FixedUpdate()
        {

        }

        //This function is called every time a scene is loaded. this behaviour is defined in Awake().
        private void SceneLoaded(VTOLScenes scene)
        {
            //If you want something to happen in only one (or more) scenes, this is where you define it.

            //For example, lets say you're making a mod which only does something in the ready room and the loading scene. This is how your code could look:
            switch (scene)
            {
                case VTOLScenes.ReadyRoom:
                    //Add your ready room code here
                    break;
                case VTOLScenes.LoadingScene:
                    //Add your loading scene code here
                    break;
            }
        }

        public void DebugMessageReceived(string message)
        {
            _Log($"Message from Named Pipe Client : {message}");
            DebugCommand cmd = DebugCommand.ParseCommand(message);
            if (cmd != null)
            {
                _Log($"Command {message} added to the list");
                DebugCommand.InvokeAsync(cmd.Run);
            }
            else
            {
                _Log($"Command {message} Invalid");
            }

        }

        public void _Log(object message)
        {
            if (message != null)
            {
                Log($"{DateTime.UtcNow.ToString("o")} - {message}");

                //if (_debugServer != null) _debugServer.SendMessage($"VTOL Spy Log Message : {message}");
            }

        }
    }
}