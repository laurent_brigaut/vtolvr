﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTOLSpy
{
    public interface ILogger
    {
        void _Log(object message);
    }
}
