#Mods and tools for VTOL VR Game

##MOD : VTOL HOTAS Driver
Map your HOTAS inputs to almost every control available in the planes!

You will be able to map any of your physical device button or axis to alomst every control available in the aircrafts on top of the stick and the throttle such as Flaps, Landing Gear, and much more

###Supported aircrafts
* AV-42C
* F/A-26B
* F-45A
* AH-94

###Setup
After installing the mod following the mod loader instructions, you will need to configure mappings. This is done through editing the Settings.xml file located in the mod installation folder.

Instructions are provided in the xml file that includes full mappings with Logitech X56 HOTAS and rudder pedals.

##MOD : VTOL Spy
Explore VTOL VR scene objects live!

##TOOL : VTOL Hotas Tool

Test your controllers and find button and controls names that you will use in VTOL HOTAS Driver settings

##Notes
Mods and tools built using Microsoft Visual Studio 2022 Community Edition
Mods can be installed using [VTOL VR Mod Loader](https://vtolvr-mods.com/) 

##License
The code is available under the MIT license